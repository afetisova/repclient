﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Xml.Serialization;
using RepClient.ReplicationServiceReference;
using FirebirdSql.Data.FirebirdClient;
using NLog;


namespace RepClient
{
    class Program
    {
        //public static string ReplaceDots(string s)
        //{
        //    string r;
        //    return s.Replace()
        //}

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static ClientSettings _ClientSettings;

        public static bool EntityFound(string entityName, string entityId, DateTime entityDtModify)
        {
            bool result = false;
            string cmdText = null;

            if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
            {
                FbConnection conn = new FbConnection(_ClientSettings.ClientConnectionString);

                if (entityName.ToUpper() == "PROTOCOLS")
                {
                    cmdText = 
                        @"select count(*) 
                          from RES_PROTOCOL_IZM_TP P 
                          where P.id_protocol=" + entityId + 
                          " AND coalesce(P.dt_modify, '1990-01-01') <'" + entityDtModify.ToShortDateString() + "'";
                }

                if (entityName.ToUpper() == "ACTS")
                {
                    cmdText = "";
                }

                if (entityName.ToUpper() == "RKO")
                {
                    cmdText = "";
                }

                if (entityName.ToUpper() == "EMPLOYEE")
                {
                    cmdText = "";
                }

                if (entityName.ToUpper() == "SUBDIVISIONS")
                {
                    cmdText = "";
                }

                if (entityName.ToUpper() == "PLANS")
                {
                    cmdText = "";
                }

                if (entityName.ToUpper() == "MKRK")
                {
                    cmdText = 
                        @"select count(*) 
                          from MON_OBJECTS O 
                          where O.ID_MON_OBJECTS = " + entityId +
                          " AND coalesce(O.LAST_TIME , '1990-01-01') < '" + entityDtModify.ToShortDateString() + "'";
                }

                FbCommand data = new FbCommand(cmdText, conn);
                conn.Open();
                FbDataReader reader = data.ExecuteReader();
                reader.Read();
                int cnt = reader.GetInt32(0);
                result = cnt > 0;
                reader.Close();
                conn.Close();
            }

            if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
            {
                SqlConnection conn = new SqlConnection(_ClientSettings.ClientConnectionString);

                if (entityName.ToUpper() == "PROTOCOLS")
                {
                    cmdText = 
                        @"select * 
                          from RK_PROTOCOLS P 
                          where P.id_protocol=" + entityId + 
                          " AND ((P.dt_modify < '" + entityDtModify.ToShortDateString() + @"') OR (P.dt_modify is null))";
                }

                if (entityName.ToUpper() == "ACTS")
                {
                    cmdText = 
                        @"select * 
                          from RK_ACT_DATA A 
                          where A.id_act_data = " + entityId + 
                          " AND ((A.dt_modify < '" + entityDtModify.ToShortDateString() + @"') OR (A.dt_modify is null))";
                }

                if (entityName.ToUpper() == "RKO")
                {
                    cmdText = 
                        @"select * 
                          from RK_RKO R 
                          where R.ID_RKO = " + entityId + 
                          " AND ((R.dt_modify < '" + entityDtModify.ToShortDateString() + @"') OR (R.dt_modify is null))";
                }

                if (entityName.ToUpper() == "EMPLOYEE")
                {
                    cmdText = 
                        @"select * 
                          from RK_SOTRUDNIK S 
                          where S.ID_SOTRUDNIK = " + entityId + 
                          " AND ((S.dt_modify < '" + entityDtModify.ToShortDateString() + @"') OR (S.dt_modify is null))";
                }

                if (entityName.ToUpper() == "SUBDIVISIONS")
                {
                    cmdText = "";
                }

                if (entityName.ToUpper() == "PLANS")
                {
                    cmdText = 
                        @"select * 
                          from RK_AGGR_PLANDETAIL P 
                          where P.ID_RK_AGGR_PLANDETAIL = " + entityId + 
                          " AND ((P.dt_modify < '" + entityDtModify.ToShortDateString() + @"') OR (P.dt_modify is null))";
                }

                if (entityName.ToUpper() == "MKRK")
                {
                    cmdText = 
                        @"select * 
                          from MON_OBJECTS O 
                          where O.ID_MON_OBJECTS = " + entityId + 
                          " AND coalesce(O.LAST_TIME , '1990-01-01') > '" + entityDtModify.ToShortDateString() + "'";
                }

                SqlCommand data = new SqlCommand(cmdText, conn);
                conn.Open();

                try
                {
                    SqlDataReader reader = data.ExecuteReader();

                    if (reader.HasRows)
                    {
                        result = true;
                    }
                    else result = false;
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при поиске существующей сущности " + entityName +
                                      " в БД-реципиенте");
                    logger.Error(e.Message);
                    /*Console.WriteLine("Произошла ошибка при поиске существующей сущности " + entityName +
                                      " в БД-реципиенте");
                    Console.WriteLine(e.Message);
                    Log.Write("Произошла ошибка при поиске существующей сущности " + entityName +
                                      " в БД-реципиенте");
                    Log.Write(e.Message);*/
                    result = false;
                }

                conn.Close();
            }

            return result;
        }

        public static bool RecordFound(string tableName, string keyFieldName, string keyFieldValue)
        {
            bool result = false;
            string cmdText = "";

            if (keyFieldValue != "")
            {
                if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
                {

                }

                if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
                {
                    SqlConnection conn = new SqlConnection(_ClientSettings.ClientConnectionString);

                    cmdText = "select " + keyFieldName + 
                             " from " + tableName + 
                             " where " + keyFieldName + " = " +
                                keyFieldValue;

                    SqlCommand data = new SqlCommand(cmdText, conn);

                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Проверяем существование " + keyFieldName + 
                                        " в таблице " + tableName + 
                                        " со значением " + keyFieldValue);
                    }

                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace(cmdText);
                    }

                    conn.Open();

                    try
                    {
                        SqlDataReader reader = data.ExecuteReader();

                        if (reader.HasRows)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при обращении к таблице " + tableName);
                        logger.Error(e.Message);
                        //Console.WriteLine("Произошла ошибка при обращении к таблице " + tableName);
                        //Console.WriteLine(e.Message);
                        //Log.Write("Произошла ошибка при обращении к таблице " + tableName);
                        //Log.Write(e.Message);
                        result = false;
                    }

                    conn.Close();
                }
            }

            return result;
        }

        public static DateTime GetMaxDate(string entity, string regionId)
        {
            //var connsb = new SqlConnectionStringBuilder()
            //SqlConnection conn = new SqlConnection("server=ZLOJ-PC\\SRVZERO;database=ASRK_RF_MSK;uid=sa;pwd=123");
            DateTime dt = new DateTime(2099, 12, 12);
            string cmdText = "";

            if (entity.ToUpper() == "PROTOCOLS")
            {
                if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RK_PROTOCOLS";
                }

                if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RES_PROTOCOL_IZM_TP";
                }
            }

            if (entity.ToUpper() == "ACTS")
            {
                if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RK_ACT_DATA";
                }

                if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from ACT_DATA";
                }
            }

            if (entity.ToUpper()=="RKO")
            {
                if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RK_RKO";
                }

                if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from rk_sr_izm";
                }
            }

            if (entity.ToUpper() == "PLANS")
            {
                if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RK_AGGR_PLANDETAIL";
                }

                if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RK_AGGR_PLANDETAIL";
                }
            }

            if (entity.ToUpper() == "EMPLOYEE")
            {
                if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RK_SOTRUDNIK";
                }

                if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from SOTRUDNIK";
                }
            }

            if ((entity.ToUpper() == "MKRK")||(entity.ToUpper() == "MKRKSERVER"))
            {
                if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select coalesce(max(last_time), '1990-01-01') as max_dt_modify 
                          from MON_OBJECTS";
                }

                if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select coalesce(max(last_time), '1990-01-01') as max_dt_modify 
                          from MON_OBJECTS";
                }
            }
            
            if (entity.ToUpper() == "SUBDIVIONS")
            {
                if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RK_SUBDIVISION";
                }

                if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from PODRAZDELENIE";
                }
            }

            if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
            {
                SqlConnection conn = new SqlConnection(_ClientSettings.ClientConnectionString);
                SqlCommand data = new SqlCommand(cmdText, conn);

                conn.Open();
                data.CommandTimeout = 1800000;
                SqlDataReader reader = data.ExecuteReader();
                var read = reader.Read();

                if (read)
                {
                    dt = reader.GetDateTime(0);
                }

                conn.Close();
            }

            if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
            {
                FbConnection conn = new FbConnection(_ClientSettings.ClientConnectionString);
                FbCommand data = new FbCommand(cmdText, conn);

                conn.Open();
                data.CommandTimeout = 1800000;
                FbDataReader reader = data.ExecuteReader();
                var read = reader.Read();

                if (read)
                {
                    dt = reader.GetDateTime(0);
                }

                conn.Close();
            }

            return dt;
        }

        public static int ReplicatePlan(PlanDetailEntity p, DateTime maxDate)
        {
            string sqlPlanDetail;
            string sqlPlanDetailFreq;

            if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
            {
                
            }

            if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
            {
                SqlConnection conn = new SqlConnection(_ClientSettings.ClientConnectionString);

                if (EntityFound("PLANS", p.Id, maxDate))
                {
                    //UPDATE
                    sqlPlanDetail = 
                        @"update RK_AGGR_PLANDETAIL 
                          set 
                              ID_RK_PLANDETAIL = @PARAM_ID_RK_PLANDETAIL 
                            , ID_RK_PLAN = @PARAM_ID_RK_PLAN 
                            , ID_REGION = @PARAM_ID_REGION 
                            , id_res = @PARAM_id_res 
                            , STARTDAT = @PARAM_STARTDAT 
                            , ENDDAT  = @PARAM_ENDDAT 
                            , ID_ZADACHA = @PARAM_ID_ZADACHA 
                            , ID_PROTOCOL = @PARAM_ID_PROTOCOL 
                            , ID_ACT_DATA = @PARAM_ID_ACT_DATA 
                            , ID_SP_TYPE_FREQ_TASK = @PARAM_ID_SP_TYPE_FREQ_TASK 
                            , FREQ = @PARAM_FREQ 
                            , FRQSTART = @PARAM_FRQSTART 
                            , FRQEND = @PARAM_FRQEND 
                            , STEP = @PARAM_STEP 
                            , ID_RKO = @PARAM_ID_RKO 
                            , ID_OBJECT = @PARAM_ID_OBJECT 
                            , ID_RK_RAZMERNOST = @PARAM_ID_RK_RAZMERNOST 
                            , ID_OPERATION = @PARAM_ID_OPERATION 
                            , IS_MARK_DEL = @PARAM_IS_MARK_DEL 
                            , IS_MOBILE = @PARAM_IS_MOBILE 
                            , ID_PERIOD = @PARAM_ID_PERIOD 
                            , ID_CLIENT = @PARAM_ID_CLIENT 
                            , ID_RK_PL_MOB_OBJ_PER = @PARAM_ID_RK_PL_MOB_OBJ_PER 
                            , ID_RK_PL_ST_OBJ_PER = @PARAM_ID_RK_PL_ST_OBJ_PER 
                            , ID_RK_PLAN_FRQ_DETAIL = @PARAM_ID_RK_PLAN_FRQ_DETAIL 
                            , ID_RK_PLAN_REQUEST_OBJ = @PARAM_ID_RK_PLAN_REQUEST_OBJ 
                            , ID_SOTRUDNIK = @PARAM_ID_SOTRUDNIK 
                            , ID_TRASSA = @PARAM_ID_TRASSA 
                            , ID_POINT = @PARAM_ID_POINT 
                            , ID_SANCTION_L = @PARAM_ID_SANCTION_L 
                            , ID_SANCTION = @PARAM_ID_SANCTION 
                            , ID_TYPE_IN_PLAN = @PARAM_ID_TYPE_IN_PLAN 
                            , ID_RK_MTD = @PARAM_ID_RK_MTD 
                            , TIME_CONTROL = @PARAM_TIME_CONTROL 
                            , is_kv = @PARAM_IS_KV 
                            , ID_OPERATION_KV = @PARAM_ID_OPERATION_KV 
                            , ID_RASPISANIE = @PARAM_ID_RASPISANIE 
                            , ID_RK_REQUEST_TASK = @PARAM_ID_RK_REQUEST_TASK 
                            , ID_CLIENT_DBS = @PARAM_ID_CLIENT_DBS 
                            , ID_RES_DBS = @PARAM_ID_RES_DBS 
                            , ID_CLIENT_EIS = @PARAM_ID_CLIENT_EIS 
                            , ID_RES_EIS = @PARAM_ID_RES_EIS 
                            , START_RECORD = @PARAM_START_RECORD 
                            , END_RECORD = @PARAM_END_RECORD 
                            , TITLE = @PARAM_TITLE 
                            , ID_CHANNEL = @PARAM_ID_CHANNEL 
                            , ID_RK_PLAN_RECORD = @PARAM_ID_RK_PLAN_RECORD 
                            , ID_RK_PLAN_MONITORING = @PARAM_ID_RK_PLAN_MONITORING 
                            , ID_RK_REQUEST_TASK_RES = @PARAM_ID_RK_REQUEST_TASK_RES 
                            , ID_SP_TYPE_PLAN_TASK = @PARAM_ID_SP_TYPE_PLAN_TASK 
                            , DT_MODIFY = @PARAM_DT_MODIFY 
                            , COUNT_RES = @PARAM_COUNT_RES 
                            , COUNT_FRQ = @PARAM_COUNT_FRQ 
                            , RPL_ID_SERVER = @PARAM_RPL_ID_SERVER 
                            , F_STRING = @PARAM_F_STRING 
                            , REQUEST_NUM = @PARAM_REQUEST_NUM 
                            , REQUEST_DATE = @PARAM_REQUEST_DATE 
                          WHERE ID_RK_AGGR_PLANDETAIL = " + p.Id;
                }
                else
                {
                    //INSERT
                    sqlPlanDetail = 
                        @"insert into RK_AGGR_PLANDETAIL 
                             (ID_RK_AGGR_PLANDETAIL, ID_RK_PLANDETAIL 
                            , ID_RK_PLAN, ID_REGION 
                            , id_res, STARTDAT 
                            , ENDDAT, ID_ZADACHA 
                            , ID_PROTOCOL, ID_ACT_DATA 
                            , ID_SP_TYPE_FREQ_TASK, FREQ 
                            , FRQSTART, FRQEND 
                            , STEP, ID_RKO 
                            , ID_OBJECT, ID_RK_RAZMERNOST 
                            , ID_OPERATION, IS_MARK_DEL 
                            , IS_MOBILE, ID_PERIOD 
                            , ID_CLIENT, ID_RK_PL_MOB_OBJ_PER 
                            , ID_RK_PL_ST_OBJ_PER, ID_RK_PLAN_FRQ_DETAIL 
                            , ID_RK_PLAN_REQUEST_OBJ, ID_SOTRUDNIK 
                            , ID_TRASSA, ID_POINT 
                            , ID_SANCTION_L, ID_SANCTION 
                            , ID_TYPE_IN_PLAN, ID_RK_MTD 
                            , TIME_CONTROL, is_kv 
                            , ID_OPERATION_KV, ID_RASPISANIE 
                            , ID_RK_REQUEST_TASK, ID_CLIENT_DBS 
                            , ID_RES_DBS, ID_CLIENT_EIS 
                            , ID_RES_EIS, START_RECORD 
                            , END_RECORD, TITLE 
                            , ID_CHANNEL, ID_RK_PLAN_RECORD 
                            , ID_RK_PLAN_MONITORING, ID_RK_REQUEST_TASK_RES 
                            , ID_SP_TYPE_PLAN_TASK, DT_MODIFY 
                            , COUNT_RES, COUNT_FRQ 
                            , RPL_ID_SERVER, F_STRING 
                            , REQUEST_NUM, REQUEST_DATE) 
                          VALUES 
                             (@PARAM_ID_RK_AGGR_PLANDETAIL, @PARAM_ID_RK_PLANDETAIL 
                            , @PARAM_ID_RK_PLAN, @PARAM_ID_REGION 
                            , @PARAM_id_res, @PARAM_STARTDAT 
                            , @PARAM_ENDDAT, @PARAM_ID_ZADACHA 
                            , @PARAM_ID_PROTOCOL, @PARAM_ID_ACT_DATA 
                            , @PARAM_ID_SP_TYPE_FREQ_TASK, @PARAM_FREQ 
                            , @PARAM_FRQSTART, @PARAM_FRQEND 
                            , @PARAM_STEP, @PARAM_ID_RKO 
                            , @PARAM_ID_OBJECT, @PARAM_ID_RK_RAZMERNOST 
                            , @PARAM_ID_OPERATION, @PARAM_IS_MARK_DEL 
                            , @PARAM_IS_MOBILE, @PARAM_ID_PERIOD 
                            , @PARAM_ID_CLIENT, @PARAM_ID_RK_PL_MOB_OBJ_PER 
                            , @PARAM_ID_RK_PL_ST_OBJ_PER, @PARAM_ID_RK_PLAN_FRQ_DETAIL 
                            , @PARAM_ID_RK_PLAN_REQUEST_OBJ, @PARAM_ID_SOTRUDNIK 
                            , @PARAM_ID_TRASSA, @PARAM_ID_POINT 
                            , @PARAM_ID_SANCTION_L, @PARAM_ID_SANCTION 
                            , @PARAM_ID_TYPE_IN_PLAN, @PARAM_ID_RK_MTD 
                            , @PARAM_TIME_CONTROL, @PARAM_is_kv 
                            , @PARAM_ID_OPERATION_KV, @PARAM_ID_RASPISANIE 
                            , @PARAM_ID_RK_REQUEST_TASK, @PARAM_ID_CLIENT_DBS 
                            , @PARAM_ID_RES_DBS, @PARAM_ID_CLIENT_EIS 
                            , @PARAM_ID_RES_EIS, @PARAM_START_RECORD 
                            , @PARAM_END_RECORD, @PARAM_TITLE 
                            , @PARAM_ID_CHANNEL, @PARAM_ID_RK_PLAN_RECORD 
                            , @PARAM_ID_RK_PLAN_MONITORING, @PARAM_ID_RK_REQUEST_TASK_RES 
                            , @PARAM_ID_SP_TYPE_PLAN_TASK, @PARAM_DT_MODIFY 
                            , @PARAM_COUNT_RES, @PARAM_COUNT_FRQ 
                            , @PARAM_RPL_ID_SERVER, @PARAM_F_STRING 
                            , @PARAM_REQUEST_NUM, @PARAM_REQUEST_DATE)";
                }

                SqlCommand addCommand = new SqlCommand(sqlPlanDetail, conn);
                //сначала проверяем и вставляем по цепочке связанные таблицы верхнего уровня
                conn.Open();

                if ((p.IdTask!="") && (!RecordFound("RK_REQUEST_TASK", "ID_RK_REQUEST_TASK", p.IdTask)))
                {
                    addCommand.CommandText = 
                        @"insert into RK_REQUEST_TASK 
                             (ID_RK_REQUEST_TASK
                            , ID_RK_ZADACHA
                            , RPL_ID_SERVER) 
                          values 
                             (@PARAM_ID_RK_REQUEST_TASK
                            , @PARAM_ID_RK_ZADACHA
                            , @PARAM_RPL_ID_SERVER)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_REQUEST_TASK", String.IsNullOrEmpty(p.IdTask) ? Convert.DBNull : p.IdTask);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_ZADACHA", String.IsNullOrEmpty(p.IdZadacha) ? Convert.DBNull : p.IdZadacha);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(p.RplIdServer) ? "-1" : p.RplIdServer);

                    try
                    {
                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("При вставке записи в RK_REQUEST_TASK с ID = " + p.IdTask + 
                                        " произошла ошибка");
                        logger.Error(e.Message);
                        //Console.WriteLine("При вставке записи в RK_REQUEST_TASK с ID = " + p.IdTask+" произошла ошибка");
                        //Console.WriteLine(e.Message);
                        //Log.Write("При вставке записи в RK_REQUEST_TASK с ID = " + p.IdTask + " произошла ошибка");
                        //Log.Write(e.Message);
                    }
                }

                if ((p.IdZadacha != "")&&(!RecordFound("RK_ZADACHA", "ID_RK_ZADACHA", p.IdZadacha)))
                {
                    addCommand.CommandText = 
                        @"insert into RK_ZADACHA 
                             (ID_RK_ZADACHA
                            , RPL_ID_SERVER) 
                          values 
                             (@PARAM_ID_RK_ZADACHA
                            , @PARAM_RPL_ID_SERVER)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_ZADACHA", String.IsNullOrEmpty(p.IdZadacha) ? Convert.DBNull : p.IdZadacha);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(p.RplIdServer) ? "-1" : p.RplIdServer);

                    try
                    {
                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("При вставке записи в RK_ZADACHA с ID = " + p.IdZadacha + 
                                        " произошла ошибка");
                        logger.Error(e.Message);
                        //Console.WriteLine("При вставке записи в RK_ZADACHA с ID = " + p.IdZadacha + " произошла ошибка");
                        //Console.WriteLine(e.Message);
                        //Log.Write("При вставке записи в  RK_ZADACHA с ID = " + p.IdZadacha + " произошла ошибка");
                        //Log.Write(e.Message);
                    }
                }

                if ((p.IdPlan!="")&& (!RecordFound("RK_PLAN", "id_rk_plan", p.IdPlan)))
                {
                    addCommand.CommandText =
                        @"INSERT INTO RK_PLAN 
                             (id_rk_plan
                            , RPL_ID_SERVER) 
                          VALUES 
                             (@PARAM_id_rk_plan
                            , @PARAM_RPL_ID_SERVER)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_id_rk_plan",
                        String.IsNullOrEmpty(p.IdPlan) ? Convert.DBNull : p.IdPlan);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(p.RplIdServer) ? "-1" : p.RplIdServer);
                    try
                    {
                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("При вставке записи в RK_PLAN с ID = " + p.IdPlan + 
                                        " произошла ошибка");
                        logger.Error(e.Message);
                        //Console.WriteLine("При вставке записи в RK_PLAN с ID = " + p.IdPlan + " произошла ошибка");
                        //Console.WriteLine(e.Message);
                        //Log.Write("При вставке записи в  RK_PLAN с ID = " + p.IdPlan + " произошла ошибка");
                        //Log.Write(e.Message);
                    }
                }

                if (!RecordFound("RK_PLANDETAIL", "ID_RK_PLANDETAILS", p.IdPlanDetailBd))
                {
                    addCommand.CommandText =
                        @"INSERT INTO  RK_PLANDETAIL 
                             (id_rk_plan
                            , ID_RK_PLANDETAILS
                            , ID_RK_REQUEST_TASK
                            , ID_RK_PL_MOB_OBJ_PER
                            , ID_RK_PLAN_REQUEST_OBJ
                            , ID_ZADACHA
                            , RPL_ID_SERVER) 
                          VALUES 
                             (@PARAM_id_rk_plan
                            , @PARAM_ID_RK_PLANDETAILS
                            , @PARAM_ID_RK_REQUEST_TASK
                            , @PARAM_ID_RK_PL_MOB_OBJ_PER
                            , @PARAM_ID_RK_PLAN_REQUEST_OBJ
                            , @PARAM_ID_ZADACHA
                            , @PARAM_RPL_ID_SERVER)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_id_rk_plan",
                        String.IsNullOrEmpty(p.IdPlan) ? Convert.DBNull : p.IdPlan);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PLANDETAILS", p.IdPlanDetailBd);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_REQUEST_TASK",
                        String.IsNullOrEmpty(p.IdTask) ? Convert.DBNull : p.IdTask);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PL_MOB_OBJ_PER",
                        String.IsNullOrEmpty(p.IdMobObjPeriod) ? Convert.DBNull : p.IdMobObjPeriod);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PLAN_REQUEST_OBJ",
                        String.IsNullOrEmpty(p.IdRequestObj) ? Convert.DBNull : p.IdRequestObj);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_ZADACHA",
                        String.IsNullOrEmpty(p.IdZadacha) ? Convert.DBNull : p.IdZadacha);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(p.RplIdServer) ? "-1" : p.RplIdServer);

                    try
                    {
                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("При вставке записи из  RK_PLANDETAIL с ID = " + p.Id + 
                                        " произошла ошибка");
                        logger.Error(e.Message);
                        //Console.WriteLine("При вставке записи из  RK_PLANDETAIL с ID = " + p.Id + " произошла ошибка");
                        //Console.WriteLine(e.Message);
                        //Log.Write("При вставке записи из  RK_PLANDETAIL с ID = " + p.Id + " произошла ошибка");
                        //Log.Write(e.Message);
                    }
                }

                addCommand.Parameters.Clear();
                addCommand.CommandText = sqlPlanDetail;
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_AGGR_PLANDETAIL", p.Id);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PLANDETAIL",
                    String.IsNullOrEmpty(p.IdPlanDetailBd) ? Convert.DBNull : p.IdPlanDetailBd);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PLAN",
                    String.IsNullOrEmpty(p.IdPlan) ? Convert.DBNull : p.IdPlan);
                addCommand.Parameters.AddWithValue("@PARAM_ID_REGION",
                    String.IsNullOrEmpty(p.IdRegion) ? Convert.DBNull : p.IdRegion);
                addCommand.Parameters.AddWithValue("@PARAM_id_res",
                    String.IsNullOrEmpty(p.IdRes) ? Convert.DBNull : p.IdRes);
                addCommand.Parameters.AddWithValue("@PARAM_STARTDAT", p.DtStart);
                addCommand.Parameters.AddWithValue("@PARAM_ENDDAT", p.DtEnd);
                addCommand.Parameters.AddWithValue("@PARAM_ID_ZADACHA",
                    String.IsNullOrEmpty(p.IdZadacha) ? Convert.DBNull : p.IdZadacha);
                addCommand.Parameters.AddWithValue("@PARAM_ID_PROTOCOL",
                    String.IsNullOrEmpty(p.IdProtocol) ? Convert.DBNull : p.IdProtocol);
                addCommand.Parameters.AddWithValue("@PARAM_ID_ACT_DATA",
                    String.IsNullOrEmpty(p.IdAct) ? Convert.DBNull : p.IdAct);
                addCommand.Parameters.AddWithValue("@PARAM_ID_SP_TYPE_FREQ_TASK",
                    String.IsNullOrEmpty(p.IdFreqTask) ? Convert.DBNull : p.IdFreqTask);
                addCommand.Parameters.AddWithValue("@PARAM_FREQ",
                    String.IsNullOrEmpty(p.Freq) ? Convert.DBNull : p.Freq.Replace(",", "."));
                addCommand.Parameters.AddWithValue("@PARAM_FRQSTART",
                    String.IsNullOrEmpty(p.FreqStart) ? Convert.DBNull : p.FreqStart.Replace(",", "."));
                addCommand.Parameters.AddWithValue("@PARAM_FRQEND",
                    String.IsNullOrEmpty(p.FreqEnd) ? Convert.DBNull : p.FreqEnd.Replace(",", "."));
                addCommand.Parameters.AddWithValue("@PARAM_STEP",
                    String.IsNullOrEmpty(p.FreqStep) ? Convert.DBNull : p.FreqStep.Replace(",", "."));
                addCommand.Parameters.AddWithValue("@PARAM_ID_RKO",
                    String.IsNullOrEmpty(p.IdRko) ? Convert.DBNull : p.IdRko);
                addCommand.Parameters.AddWithValue("@PARAM_ID_OBJECT",
                    String.IsNullOrEmpty(p.IdObject) ? Convert.DBNull : p.IdObject);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_RAZMERNOST",
                    String.IsNullOrEmpty(p.IdRazm) ? Convert.DBNull : p.IdRazm);
                addCommand.Parameters.AddWithValue("@PARAM_ID_OPERATION",
                    String.IsNullOrEmpty(p.IdOperation) ? Convert.DBNull : p.IdOperation);
                addCommand.Parameters.AddWithValue("@PARAM_IS_MARK_DEL",
                    String.IsNullOrEmpty(p.IsMarkDel) ? Convert.DBNull : p.IsMarkDel);
                addCommand.Parameters.AddWithValue("@PARAM_IS_MOBILE",
                    String.IsNullOrEmpty(p.IsMobile) ? Convert.DBNull : p.IsMobile);
                addCommand.Parameters.AddWithValue("@PARAM_ID_PERIOD",
                    String.IsNullOrEmpty(p.IdPeriod) ? Convert.DBNull : p.IdPeriod);
                addCommand.Parameters.AddWithValue("@PARAM_ID_CLIENT",
                    String.IsNullOrEmpty(p.IdClient) ? Convert.DBNull : p.IdClient);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PL_MOB_OBJ_PER",
                    String.IsNullOrEmpty(p.IdMobObjPeriod) ? Convert.DBNull : p.IdMobObjPeriod);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PL_ST_OBJ_PER",
                    String.IsNullOrEmpty(p.IdStObjPeriod) ? Convert.DBNull : p.IdStObjPeriod);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PLAN_FRQ_DETAIL",
                    String.IsNullOrEmpty(p.IdFrqDetail) ? Convert.DBNull : p.IdFrqDetail);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PLAN_REQUEST_OBJ",
                    String.IsNullOrEmpty(p.IdRequestObj) ? Convert.DBNull : p.IdRequestObj);
                addCommand.Parameters.AddWithValue("@PARAM_ID_SOTRUDNIK",
                    String.IsNullOrEmpty(p.IdSotrudnik) ? Convert.DBNull : p.IdSotrudnik);
                addCommand.Parameters.AddWithValue("@PARAM_ID_TRASSA",
                    String.IsNullOrEmpty(p.IdTrassa) ? Convert.DBNull : p.IdTrassa);
                addCommand.Parameters.AddWithValue("@PARAM_ID_POINT",
                    String.IsNullOrEmpty(p.IdPoint) ? Convert.DBNull : p.IdPoint);
                addCommand.Parameters.AddWithValue("@PARAM_ID_SANCTION_L",
                    String.IsNullOrEmpty(p.IdSanctionL) ? Convert.DBNull : p.IdSanctionL);
                addCommand.Parameters.AddWithValue("@PARAM_ID_SANCTION",
                    String.IsNullOrEmpty(p.IdSanction) ? Convert.DBNull : p.IdSanction);
                addCommand.Parameters.AddWithValue("@PARAM_ID_TYPE_IN_PLAN",
                    String.IsNullOrEmpty(p.IdType) ? Convert.DBNull : p.IdType);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_MTD",
                    String.IsNullOrEmpty(p.IdMtd) ? Convert.DBNull : p.IdMtd);
                addCommand.Parameters.AddWithValue("@PARAM_TIME_CONTROL",
                    String.IsNullOrEmpty(p.TimeControl) ? Convert.DBNull : p.TimeControl.Replace(",","."));
                addCommand.Parameters.AddWithValue("@PARAM_is_kv",
                    String.IsNullOrEmpty(p.IsKv) ? Convert.DBNull : p.IsKv);
                addCommand.Parameters.AddWithValue("@PARAM_ID_OPERATION_KV",
                    String.IsNullOrEmpty(p.IdOperationKv) ? Convert.DBNull : p.IdOperationKv);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RASPISANIE",
                    String.IsNullOrEmpty(p.IdRaspisanie) ? Convert.DBNull : p.IdRaspisanie);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_REQUEST_TASK",
                    String.IsNullOrEmpty(p.IdTask) ? Convert.DBNull : p.IdTask);
                addCommand.Parameters.AddWithValue("@PARAM_ID_CLIENT_DBS",
                    String.IsNullOrEmpty(p.IdClientDbs) ? Convert.DBNull : p.IdClientDbs);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RES_DBS",
                    String.IsNullOrEmpty(p.IdResDbs) ? Convert.DBNull : p.IdResDbs);
                addCommand.Parameters.AddWithValue("@PARAM_ID_CLIENT_EIS",
                    String.IsNullOrEmpty(p.IdClientEis) ? Convert.DBNull : p.IdClientEis);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RES_EIS",
                    String.IsNullOrEmpty(p.IdResEis) ? Convert.DBNull : p.IdResEis);
                addCommand.Parameters.AddWithValue("@PARAM_START_RECORD",
                    String.IsNullOrEmpty(p.StartRec) ? Convert.DBNull : p.StartRec);
                addCommand.Parameters.AddWithValue("@PARAM_END_RECORD",
                    String.IsNullOrEmpty(p.EndRec) ? Convert.DBNull : p.EndRec);
                addCommand.Parameters.AddWithValue("@PARAM_TITLE",
                    String.IsNullOrEmpty(p.Title) ? Convert.DBNull : p.Title);
                addCommand.Parameters.AddWithValue("@PARAM_ID_CHANNEL",
                    String.IsNullOrEmpty(p.IdChannel) ? Convert.DBNull : p.IdChannel);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PLAN_RECORD",
                    String.IsNullOrEmpty(p.IdPlanRec) ? Convert.DBNull : p.IdPlanRec);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PLAN_MONITORING",
                    String.IsNullOrEmpty(p.IdPlanMon) ? Convert.DBNull : p.IdPlanMon);
                addCommand.Parameters.AddWithValue("@PARAM_ID_RK_REQUEST_TASK_RES",
                    String.IsNullOrEmpty(p.IdTaskRes) ? Convert.DBNull : p.IdTaskRes);
                addCommand.Parameters.AddWithValue("@PARAM_ID_SP_TYPE_PLAN_TASK",
                    String.IsNullOrEmpty(p.IdTypePlanTask) ? Convert.DBNull : p.IdTypePlanTask);
                addCommand.Parameters.AddWithValue("@PARAM_DT_MODIFY", p.DtModify);
                addCommand.Parameters.AddWithValue("@PARAM_COUNT_RES",
                    String.IsNullOrEmpty(p.CountRes) ? Convert.DBNull : p.CountRes);
                addCommand.Parameters.AddWithValue("@PARAM_COUNT_FRQ",
                    String.IsNullOrEmpty(p.CountFrq) ? Convert.DBNull : p.CountFrq);
                addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(p.RplIdServer) ? "-1" : p.RplIdServer);
                addCommand.Parameters.AddWithValue("@PARAM_F_STRING",
                    String.IsNullOrEmpty(p.FString) ? Convert.DBNull : p.FString.Replace(",", ";"));
                addCommand.Parameters.AddWithValue("@PARAM_REQUEST_NUM",
                    String.IsNullOrEmpty(p.NumRequest) ? Convert.DBNull : p.NumRequest);
                addCommand.Parameters.AddWithValue("@PARAM_REQUEST_DATE", p.DateRequest);

                try
                {
                    //foreach (SqlParameter param in addCommand.Parameters)
                    //{
                    //    Log.Write(param.ParameterName + " = " + param.Value.ToString());
                    //}
                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("При вставке записи из RK_AGGR_PLANDETAIL с ID = " + p.Id + 
                                    " произошла ошибка");
                    logger.Error(e.Message);
                    foreach (SqlParameter param in addCommand.Parameters)
                    {
                        logger.Error(param.ParameterName + " = " + param.Value.ToString());
                    }
                    //Console.WriteLine("При вставке записи из RK_AGGR_PLANDETAIL с ID = " + p.Id + " произошла ошибка");
                    //Console.WriteLine(e.Message);
                    //Log.Write("При вставке записи из RK_AGGR_PLANDETAIL с ID = " + p.Id + " произошла ошибка");
                    //Log.Write(e.Message);
                    //foreach (SqlParameter param in addCommand.Parameters)
                    //{
                    //    Log.Write(param.ParameterName+" = "+param.Value.ToString());
                    //}
                }

                addCommand.CommandText = 
                    @"delete from RK_AGGR_PLANDETAIL_FRQ 
                      where ID_RK_AGGR_PLANDETAIL = " + p.Id;

                try
                {
                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при удалении записи из RK_AGGR_PLANDETAIL_FRQ с ID_RK_AGGR_PLANDETAIL = " + p.Id);
                    //Log.Write("Произошла ошибка при удалении записи из RK_AGGR_PLANDETAIL_FRQ с ID_RK_AGGR_PLANDETAIL = " + p.Id);
                }

                sqlPlanDetailFreq = 
                    @"insert into RK_AGGR_PLANDETAIL_FRQ  
                         (ID_RK_AGGR_PLANDETAIL
                        , ID_RK_AGGR_PLANDETAIL_FRQ
                        , ID_RK_PLAN_FRQ_LIST
                        , FREQ, FREQ_INT
                        , FREQ_WIDTH
                        , FREQ_WIDTH_INT
                        , ID_RK_RAZMERNOST
                        , LOFREQ
                        , HIFREQ
                        , FRQ_STEP
                        , ID_SP_BAND_DIAP
                        , ID_SP_BAND_FRQ
                        , LOFREQ_INT
                        , HIFREQ_INT
                        , FRQ_STEP_INT
                        , RPL_ID_SERVER) 
                      VALUES 
                         (@PARAM_ID_RK_AGGR_PLANDETAIL
                        , @PARAM_ID_RK_AGGR_PLANDETAIL_FRQ
                        , @PARAM_ID_RK_PLAN_FRQ_LIST
                        , @PARAM_FREQ
                        , @PARAM_FREQ_INT
                        , @PARAM_FREQ_WIDTH
                        , @PARAM_FREQ_WIDTH_INT
                        , @PARAM_ID_RK_RAZMERNOST
                        , @PARAM_LOFREQ
                        , @PARAM_HIFREQ
                        , @PARAM_FRQ_STEP
                        , @PARAM_ID_SP_BAND_DIAP
                        , @PARAM_ID_SP_BAND_FRQ
                        , @PARAM_LOFREQ_INT
                        , @PARAM_HIFREQ_INT
                        , @PARAM_FRQ_STEP_INT
                        , @PARAM_RPL_ID_SERVER)";

                addCommand.CommandText = sqlPlanDetailFreq;

                foreach (CloudPlanDetailFreq freq in p.PlanDetailFreq)
                {
                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_AGGR_PLANDETAIL", p.Id);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_AGGR_PLANDETAIL_FRQ", freq.Id);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PLAN_FRQ_LIST",
                        String.IsNullOrEmpty(freq.IdFrqList) ? Convert.DBNull : freq.IdFrqList);
                    addCommand.Parameters.AddWithValue("@PARAM_FREQ",
                        String.IsNullOrEmpty(freq.Freq) ? Convert.DBNull : freq.Freq.Replace(",","."));
                    addCommand.Parameters.AddWithValue("@PARAM_FREQ_INT",
                        String.IsNullOrEmpty(freq.FreqInt) ? Convert.DBNull : freq.FreqInt.Replace(",", "."));
                    addCommand.Parameters.AddWithValue("@PARAM_FREQ_WIDTH",
                        String.IsNullOrEmpty(freq.FreqWidth) ? Convert.DBNull : freq.FreqWidth.Replace(",", "."));
                    addCommand.Parameters.AddWithValue("@PARAM_FREQ_WIDTH_INT",
                        String.IsNullOrEmpty(freq.FreqWidthInt) ? Convert.DBNull : freq.FreqWidthInt.Replace(",", "."));
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_RAZMERNOST",
                        String.IsNullOrEmpty(freq.IdRazm) ? Convert.DBNull : freq.IdRazm);
                    addCommand.Parameters.AddWithValue("@PARAM_LOFREQ",
                        String.IsNullOrEmpty(freq.FreqStart) ? Convert.DBNull : freq.FreqStart.Replace(",", "."));
                    addCommand.Parameters.AddWithValue("@PARAM_HIFREQ",
                        String.IsNullOrEmpty(freq.FreqEnd) ? Convert.DBNull : freq.FreqEnd.Replace(",", "."));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_STEP",
                        String.IsNullOrEmpty(freq.FreqStep) ? Convert.DBNull : freq.FreqStep.Replace(",", "."));
                    addCommand.Parameters.AddWithValue("@PARAM_ID_SP_BAND_DIAP",
                        String.IsNullOrEmpty(freq.IdBandDiap) ? Convert.DBNull : freq.IdBandDiap);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_SP_BAND_FRQ",
                        String.IsNullOrEmpty(freq.IdBandFrq) ? Convert.DBNull : freq.IdBandFrq);
                    addCommand.Parameters.AddWithValue("@PARAM_LOFREQ_INT",
                        String.IsNullOrEmpty(freq.FreqStartInt) ? Convert.DBNull : freq.FreqStartInt.Replace(",", "."));
                    addCommand.Parameters.AddWithValue("@PARAM_HIFREQ_INT",
                        String.IsNullOrEmpty(freq.FreqEndInt) ? Convert.DBNull : freq.FreqEndInt.Replace(",", "."));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_STEP_INT",
                        String.IsNullOrEmpty(freq.FreqStepInt) ? Convert.DBNull : freq.FreqStepInt.Replace(",", "."));
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(freq.RplIdServer) ? "-1" : freq.RplIdServer);

                    try
                    {
                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в PLAN_DETAIL_FRQ с ID = " + freq.Id);
                        logger.Error(e.Message);
                        //Console.WriteLine("Произошла ошибка при вставке записи в PLAN_DETAIL_FRQ с ID = " + freq.Id);
                        //Console.WriteLine(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в PLAN_DETAIL_FRQ с ID =" + freq.Id);
                        //Log.Write(e.Message);
                    }

                }

                conn.Close();
            }

            return 0;
        }

        public static int ReplicateEmployee(EmployeeEntity ee, DateTime maxDate)
        {
            if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
            {
                
            }

            if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
            {
                SqlConnection conn = new SqlConnection(_ClientSettings.ClientConnectionString);

                string sqlEmployee;

                if (EntityFound("EMPLOYEE", ee.Id, maxDate))
                {
                    //UPDATE
                    sqlEmployee = 
                        @"UPDATE RK_SOTRUDNIK 
                          set 
                              famil = @param_famil
                            , name = @param_name
                            , otch = @param_otch
                            , phone = @param_phone
                            , phone_work = @param_phone_work
                            , phone_work_prn = @param_phone_work_prn
                            , email = @param_email
                            , id_otdel = @param_id_otdel
                            , l_lock = @param_l_lock
                            , date_disable = @param_date_disable
                            , id_dl = @param_id_dl
                            , dbs_id = @param_dbs_id
                            , id_polz_bd = @param_id_polz_bd
                            , sotrudnik_login = @param_sotrudnik_login
                            , ID_USER_CERTIFICATE = @param_ID_USER_CERTIFICATE
                            , dt_modify = @param_dt_modify
                            , is_deleted = @param_is_deleted
                            , rpl_id_server = @param_rpl_id_server 
                          where id_sotrudnik = " + ee.Id;
                }
                else
                {
                    //INSERT
                    sqlEmployee = 
                        @"INSERT INTO RK_SOTRUDNIK 
                             (id_sotrudnik
                            , famil
                            , name
                            , otch
                            , phone
                            , phone_work
                            , phone_work_prn
                            , email
                            , id_otdel
                            , l_lock
                            , date_disable
                            , id_dl
                            , dbs_id
                            , id_polz_bd
                            , sotrudnik_login
                            , ID_USER_CERTIFICATE
                            , dt_modify
                            , is_deleted
                            , rpl_id_server) 
                          VALUES 
                             (@param_id_sotrudnik
                            , @param_famil
                            , @param_name
                            , @param_otch
                            , @param_phone
                            , @param_phone_work
                            , @param_phone_work_prn
                            , @param_email
                            , @param_id_otdel
                            , @param_l_lock
                            , @param_date_disable
                            , @param_id_dl
                            , @param_dbs_id
                            , @param_id_polz_bd
                            , @param_sotrudnik_login
                            , @param_ID_USER_CERTIFICATE
                            , @param_dt_modify
                            , @param_is_deleted
                            , @param_rpl_id_server)";
                }

                SqlCommand addCommand = new SqlCommand(sqlEmployee, conn);
                addCommand.Parameters.Clear();
                addCommand.Parameters.AddWithValue("@param_id_sotrudnik", ee.Id);
                addCommand.Parameters.AddWithValue("@param_famil", String.IsNullOrEmpty(ee.Surname) ? Convert.DBNull : ee.Surname);
                addCommand.Parameters.AddWithValue("@param_name", String.IsNullOrEmpty(ee.Name) ? Convert.DBNull : ee.Name);
                addCommand.Parameters.AddWithValue("@param_otch",
                    String.IsNullOrEmpty(ee.SecondName) ? Convert.DBNull : ee.SecondName);
                addCommand.Parameters.AddWithValue("@param_phone",
                    String.IsNullOrEmpty(ee.Phone) ? Convert.DBNull : ee.Phone);
                addCommand.Parameters.AddWithValue("@param_phone_work",
                    String.IsNullOrEmpty(ee.PhoneWork) ? Convert.DBNull : ee.PhoneWork);
                addCommand.Parameters.AddWithValue("@param_phone_work_prn",
                    String.IsNullOrEmpty(ee.PhoneWorkPrn) ? Convert.DBNull : ee.PhoneWorkPrn);
                addCommand.Parameters.AddWithValue("@param_email",
                    String.IsNullOrEmpty(ee.Email) ? Convert.DBNull : ee.Email);
                addCommand.Parameters.AddWithValue("@param_id_otdel",
                    String.IsNullOrEmpty(ee.IdSubdivision) ? Convert.DBNull : ee.IdSubdivision);
                addCommand.Parameters.AddWithValue("@param_l_lock",
                    String.IsNullOrEmpty(ee.Lock) ? Convert.DBNull : ee.Lock);
                addCommand.Parameters.AddWithValue("@param_date_disable", ee.DisableDate);
                addCommand.Parameters.AddWithValue("@param_id_dl",
                    String.IsNullOrEmpty(ee.IdPosition) ? Convert.DBNull : ee.IdPosition);
                addCommand.Parameters.AddWithValue("@param_dbs_id",
                    String.IsNullOrEmpty(ee.IdDbs) ? Convert.DBNull : ee.IdDbs);
                addCommand.Parameters.AddWithValue("@param_id_polz_bd",
                    String.IsNullOrEmpty(ee.IdDbUser) ? Convert.DBNull : ee.IdDbUser);
                addCommand.Parameters.AddWithValue("@param_sotrudnik_login",
                    String.IsNullOrEmpty(ee.Login) ? Convert.DBNull : ee.Login);
                addCommand.Parameters.AddWithValue("@param_ID_USER_CERTIFICATE",
                    String.IsNullOrEmpty(ee.IdUserCertificate) ? Convert.DBNull : ee.IdUserCertificate);
                addCommand.Parameters.AddWithValue("@param_dt_modify", ee.DtModify);
                addCommand.Parameters.AddWithValue("@param_is_deleted",
                    String.IsNullOrEmpty(ee.IsDeleted) ? Convert.DBNull : ee.IsDeleted);
                addCommand.Parameters.AddWithValue("@param_rpl_id_server",
                        String.IsNullOrEmpty(ee.RplIdServer) ? "-1" : ee.RplIdServer);
                conn.Open();

                try
                {
                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при сохранении сущности EMPLOYEE с ID = " + ee.Id);
                    logger.Error(e.Message);
                    //Log.Write("Произошла ошибка при сохранении сущности EMPLOYEE с ID = " + ee.Id);
                    //Log.Write(e.Message);
                }

                /*foreach (CloudEmployeeCertificate certificate in ee.Certificates)
                {
                    addCommand.CommandText = "";
                    addCommand.Parameters.Clear();
                    try
                    {
                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An error has occured while inserting EMPLOYEE_CERTIFICATE with ID = " + certificate.Id);
                        Console.WriteLine(e.Message);
                        Log.Write("An error has occured while inserting EMPLOYEE_CERTIFICATE with ID = " + certificate.Id);
                        Log.Write(e.Message);
                    }
                }*/

                conn.Close();
            }

            return 0;
        }

        public static int ReplicateRko(RkoEntity r, DateTime maxDate)
        {
            string sqlRko;

            if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
            {
                
            }

            if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
            {
                SqlConnection conn = new SqlConnection(_ClientSettings.ClientConnectionString);

                if (EntityFound("RKO", r.Id, maxDate))
                {
                    //UPDATE
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Существует RKO с ID = " + r.Id);
                    }

                    sqlRko = 
                        @"update RK_RKO 
                          SET 
                              ID_RKO_COMPLEX = @PARAM_ID_RKO_COMPLEX
                            , RKO_NAME =  @PARAM_RKO_NAME
                            , RKO_DATE = @PARAM_RKO_DATE
                            , FACTORY_NUMBER = @PARAM_FACTORY_NUMBER
                            , IP_ADDRESS = @PARAM_IP_ADDRESS
                            , RKO_INV_NUMBER = @PARAM_RKO_INV_NUMBER
                            , RPL_ID_SERVER = @PARAM_RPL_ID_SERVER
                          WHERE ID_RKO = @PARAM_ID_RKO";
                }
                else
                {
                    //INSERT NEW
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Не существует RKO с ID = " + r.Id);
                    }

                    sqlRko = 
                        @"insert into rk_rko 
                             (ID_RKO
                            , ID_RKO_COMPLEX
                            , RKO_NAME
                            , RKO_DATE
                            , FACTORY_NUMBER
                            , IP_ADDRESS
                            , RKO_INV_NUMBER
                            , RPL_ID_SERVER) 
                          VALUES 
                             (@PARAM_ID_RKO
                            , @PARAM_ID_RKO_COMPLEX
                            , @PARAM_RKO_NAME
                            , @PARAM_RKO_DATE
                            , @PARAM_FACTORY_NUMBER
                            , @PARAM_IP_ADDRESS
                            , @PARAM_RKO_INV_NUMBER
                            , @PARAM_RPL_ID_SERVER)";
                }

                conn.Open();

                //проверить subdivision 
                if  (!RecordFound("RK_SUBDIVISION", "ID_SUBDIVISION", r.IdSubdivision) )
                {
                    //вставить сабдивижон
                    string sqlSubdivision =
                        @"insert into RK_SUBDIVISION 
                             (ID_SUBDIVISION
                            , SUBDIVISION_NAME
                            , RPL_ID_SERVER
                            , ID_REGION) 
                          Values
                             (@PARAM_ID_SUBDIVISION
                            , @PARAM_SUBDIVISION_NAME
                            , @PARAM_RPL_ID_SERVER
                            , @PARAM_ID_REGION)";

                    SqlCommand subdivisionCommand = new SqlCommand(sqlSubdivision, conn);
                    subdivisionCommand.Parameters.Clear();
                    subdivisionCommand.Parameters.AddWithValue("@PARAM_ID_SUBDIVISION", r.IdSubdivision);
                    subdivisionCommand.Parameters.AddWithValue("@PARAM_SUBDIVISION_NAME", String.IsNullOrEmpty(r.SubvisionName) ? Convert.DBNull : r.SubvisionName);
                    subdivisionCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(r.SubdivisionRplIdServer) ? "-1" : r.SubdivisionRplIdServer);
                    subdivisionCommand.Parameters.AddWithValue("@PARAM_ID_REGION", String.IsNullOrEmpty(r.IdRegion) ? Convert.DBNull : r.IdRegion);
                    
                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся вставить RK_SUBDIVISION");
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in subdivisionCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        subdivisionCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при сохранении сущности SUBDIVION с ID = " + r.IdSubdivision);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при сохранении сущности SUBDIVION с ID = " + r.IdSubdivision);
                        //Log.Write(e.Message);
                        foreach (SqlParameter param in subdivisionCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                    
                }

                //проверить и вставить (проапдейтить ) RK_POST
                string sqlPost;
                if (!RecordFound("RK_POST", "ID_POST", r.IdPost))
                {
                    //вставить ( ) RK_POST
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Новый RK_POST");
                    }

                    sqlPost =
                        @"insert into RK_POST 
                             (ID_POST
                            , ID_POST_CATEGORY
                            , ID_SUBDIVISION
                            , RKP_NAME
                            , RPL_ID_SERVER
                            , RKP_ADDRESS
                            , GEO_LAT
                            , GEO_LONG) 
                          VALUES 
                             (@PARAM_ID_POST
                            , @PARAM_ID_POST_CATEGORY
                            , @PARAM_ID_SUBDIVISION
                            , @PARAM_RKP_NAME
                            , @PARAM_RPL_ID_SERVER
                            , @PARAM_RKP_ADDRESS
                            , @PARAM_GEO_LAT
                            , @PARAM_GEO_LONG)";
                }
                else
                {
                    //проапдейтить RK_POST
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Существует RK_POST");
                    }

                    sqlPost = 
                        @"UPDATE RK_POST 
                          SET 
                              ID_POST_CATEGORY = @PARAM_ID_POST_CATEGORY 
                            , RKP_NAME = @PARAM_RKP_NAME 
                            , RPL_ID_SERVER = @PARAM_RPL_ID_SERVER 
                            , RKP_ADDRESS = @PARAM_RKP_ADDRESS 
                            , GEO_LAT = @PARAM_GEO_LAT 
                            , GEO_LONG = @PARAM_GEO_LONG 
                          WHERE ID_POST = " + r.IdPost;
                }

                SqlCommand postCommand = new SqlCommand(sqlPost, conn);
                postCommand.Parameters.Clear();
                postCommand.Parameters.AddWithValue("@PARAM_ID_POST", r.IdPost);
                postCommand.Parameters.AddWithValue("@PARAM_ID_POST_CATEGORY", 0);
                postCommand.Parameters.AddWithValue("@PARAM_ID_SUBDIVISION", String.IsNullOrEmpty(r.IdSubdivision) ? Convert.DBNull : r.IdSubdivision);
                postCommand.Parameters.AddWithValue("@PARAM_RKP_NAME", String.IsNullOrEmpty(r.PostName) ? Convert.DBNull : r.PostName);
                postCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(r.PostRplIdServer) ? "-1" : r.PostRplIdServer);
                postCommand.Parameters.AddWithValue("@PARAM_RKP_ADDRESS", String.IsNullOrEmpty(r.PostAddress) ? Convert.DBNull : r.PostAddress);
                postCommand.Parameters.AddWithValue("@PARAM_GEO_LAT", String.IsNullOrEmpty(r.GeoLat) ? Convert.DBNull : r.GeoLat);
                postCommand.Parameters.AddWithValue("@PARAM_GEO_LONG", String.IsNullOrEmpty(r.GeoLong) ? Convert.DBNull : r.GeoLong);
                
                try
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Пытаемся вставить RK_POST");
                    }

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in postCommand.Parameters)
                        {
                            logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    postCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при вставке записи в RK_POST с ID = " + r.IdPost);
                    logger.Error(e.Message);
                    //Log.Write("Произошла ошибка при вставке записи в RK_POST с ID = " + r.IdPost);
                    //Log.Write(e.Message);

                    foreach (SqlParameter param in postCommand.Parameters)
                    {
                        logger.Error(param.ParameterName + " = " + param.Value.ToString());
                    }
                }

                string sqlComplex;
                string sqlVendor;

                if (!RecordFound("RK_RKO_COMPLEX", "ID_RKO_COMPLEX", r.Id))
                {
                    //вставить RK_RKO_COMPLEX 
                    sqlComplex = 
                        @"INSERT INTO RK_RKO_COMPLEX 
                             (ID_RKO_COMPLEX
                            , ID_POST
                            , ID_RKO_VENDORS
                            , NOCOMPLEX
                            , RPL_ID_SERVER) 
                          VALUES 
                             (@PARAM_ID_RKO_COMPLEX
                            , @PARAM_ID_POST
                            , @PARAM_ID_RKO_VENDORS
                            , 1
                            , @PARAM_RPL_ID_SERVER)";

                    sqlVendor =
                        @"INSERT INTO RK_SP_RKO_VENDORS 
                             (ID_RKO_VENDORS
                            , VENDOR_NAME) 
                          VALUES 
                             (@PARAM_ID_RKO_VENDORS
                            , @PARAM_VENDOR_NAME)";
                }
                else
                {
                    //проапдейтить RK_RKO_COMPLEX 
                    sqlComplex = 
                        @"UPDATE RK_RKO_COMPLEX 
                          SET  
                              ID_POST = @PARAM_ID_POST
                            , ID_RKO_VENDORS = @PARAM_ID_RKO_VENDORS
                            , NOCOMPLEX = 1
                            , RPL_ID_SERVER = -1 
                          WHERE ID_RKO_COMPLEX = @PARAM_ID_RKO_COMPLEX";

                    sqlVendor =
                        @"INSERT INTO RK_SP_RKO_VENDORS 
                             (ID_RKO_VENDORS
                            , VENDOR_NAME) 
                          VALUES 
                             (@PARAM_ID_RKO_VENDORS
                            , @PARAM_VENDOR_NAME)";
                }

                SqlCommand vendorCommand = new SqlCommand(sqlVendor, conn);
                SqlCommand complexCommand = new SqlCommand(sqlComplex, conn);
                vendorCommand.Parameters.Clear();
                vendorCommand.Parameters.AddWithValue("@PARAM_ID_RKO_VENDORS", r.Id);
                vendorCommand.Parameters.AddWithValue("@PARAM_VENDOR_NAME",
                    String.IsNullOrEmpty(r.VendorName) ? "неизвестно" : r.VendorName);
                
                if (! RecordFound("RK_SP_RKO_VENDORS", "ID_RKO_VENDORS", r.Id))
                {
                    try
                    {
                        if (_ClientSettings.Verbose)
                        { 
                            logger.Trace("Пытаемся вставить RK_SP_RKO_VENDORS"); 
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in vendorCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        vendorCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в  RKO_VENDOR c ID = " + r.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в  RKO_VENDOR c ID = " + r.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in vendorCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }
                
                complexCommand.Parameters.Clear();
                complexCommand.Parameters.AddWithValue("@PARAM_ID_RKO_COMPLEX", r.Id);
                complexCommand.Parameters.AddWithValue("@PARAM_ID_POST",
                    String.IsNullOrEmpty(r.IdPost) ? Convert.DBNull : r.IdPost);
                complexCommand.Parameters.AddWithValue("@PARAM_ID_RKO_VENDORS", r.Id);
                complexCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER",
                    String.IsNullOrEmpty(r.PostRplIdServer) ? "-1" : r.PostRplIdServer);

                try
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Пытаемся вставить RK_RKO_COMPLEX");
                    }

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in complexCommand.Parameters)
                        {
                            logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    complexCommand.ExecuteNonQuery();
                }

                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при вставке записи в  RKO_COMPLEX c ID = " + r.Id);
                    logger.Error(e.Message);
                    //Log.Write("Произошла ошибка при вставке записи в  RKO_COMPLEX c ID = " + r.Id);
                    //Log.Write(e.Message);

                    foreach (SqlParameter param in complexCommand.Parameters)
                    {
                        logger.Error(param.ParameterName + " = " + param.Value.ToString());
                    }
                }

                //вставить или проапдейтить RK_RKO
                SqlCommand rkoCommand = new SqlCommand(sqlRko, conn);
                rkoCommand.Parameters.AddWithValue("@PARAM_ID_RKO_COMPLEX", r.Id);
                rkoCommand.Parameters.AddWithValue("@PARAM_RKO_NAME",
                    String.IsNullOrEmpty(r.RkoName) ? Convert.DBNull : r.RkoName);
                rkoCommand.Parameters.AddWithValue("@PARAM_RKO_DATE", r.RkoDate);
                rkoCommand.Parameters.AddWithValue("@PARAM_FACTORY_NUMBER",
                    String.IsNullOrEmpty(r.FactoryNumber) ? Convert.DBNull : r.FactoryNumber);
                rkoCommand.Parameters.AddWithValue("@PARAM_IP_ADDRESS",
                    String.IsNullOrEmpty(r.IpAddress) ? Convert.DBNull : r.IpAddress);
                rkoCommand.Parameters.AddWithValue("@PARAM_RKO_INV_NUMBER",
                    String.IsNullOrEmpty(r.InvNumber) ? Convert.DBNull : r.InvNumber);
                rkoCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER",
                                   String.IsNullOrEmpty(r.RplIdServer) ? "-1" : r.RplIdServer);
                rkoCommand.Parameters.AddWithValue("@PARAM_ID_RKO", r.Id);
                
                try
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Пытаемся вставить RKO c ID = " + r.Id);
                    }

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in rkoCommand.Parameters)
                        {
                            logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    rkoCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при вставке записи в   RKO c ID = " + r.Id);
                    logger.Error(e.Message);
                    //Log.Write("Произошла ошибка при вставке записи в  RKO c ID = " + r.Id);
                    //Log.Write(e.Message);

                    foreach (SqlParameter param in rkoCommand.Parameters)
                    {
                        logger.Error(param.ParameterName + " = " + param.Value.ToString());
                    }
                }
              
                //удалить и вставить список actrko
                rkoCommand.Parameters.Clear();
                rkoCommand.CommandText = "delete from RK_REL_ACT_RKO where ID_RKO = " + r.Id;
                rkoCommand.ExecuteNonQuery();

                foreach (CloudActRko actrko in r.Acts)
                {
                    rkoCommand.CommandText =
                        @"insert into RK_REL_ACT_RKO 
                             (ID_ACT_DATA
                            , ID_RKO) 
                          values(@PARAM_ID_ACT_DATA
                            , @PARAM_ID_RKO)";

                    rkoCommand.Parameters.Clear();
                    //rkoCommand.Parameters.AddWithValue("@PARAM_ID_REL_ACT_RKO", actrko.Id);
                    rkoCommand.Parameters.AddWithValue("@PARAM_ID_ACT_DATA", String.IsNullOrEmpty(actrko.Act) ? Convert.DBNull : actrko.Act);                    
                    rkoCommand.Parameters.AddWithValue("@PARAM_ID_RKO", r.Id);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся вставить в RK_REL_ACT_RKO  ID_RKO = " + r.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in rkoCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        rkoCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в  REL_RKO_ACT c RKO_ID = " + r.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в  REL_RKO_ACT c RKO_ID = " + r.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in rkoCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                //удалить и вставить список rko_metrology
                rkoCommand.Parameters.Clear();
                rkoCommand.CommandText = "delete from RK_RKO_METROLOGY where ID_RKO = " + r.Id;
                rkoCommand.ExecuteNonQuery();
                rkoCommand.Parameters.Clear();

                foreach (CloudRkoMetrology metrology in r.Metrology)
                {
                    rkoCommand.CommandText =
                        @"insert into RK_RKO_METROLOGY 
                             (ID_RKO_METROLOGY
                            , ID_RKO, CERT_NUM
                            , DATE_FROM
                            , DATE_TO
                            , PAGES) 
                          values 
                             (@PARAM_ID_RKO_METROLOGY
                            , @PARAM_ID_RKO
                            , @PARAM_CERT_NUM
                            , @PARAM_DATE_FROM
                            , @PARAM_DATE_TO
                            , @PARAM_PAGES)";

                    rkoCommand.Parameters.Clear();
                    rkoCommand.Parameters.AddWithValue("@PARAM_ID_RKO_METROLOGY",
                        String.IsNullOrEmpty(metrology.Id) ? Convert.DBNull : metrology.Id);
                    rkoCommand.Parameters.AddWithValue("@PARAM_ID_RKO", r.Id);
                    rkoCommand.Parameters.AddWithValue("@PARAM_CERT_NUM", String.IsNullOrEmpty(metrology.CertNum) ? Convert.DBNull : metrology.CertNum);
                    rkoCommand.Parameters.AddWithValue("@PARAM_DATE_FROM", metrology.CertDateFrom);
                    rkoCommand.Parameters.AddWithValue("@PARAM_DATE_TO", metrology.CertDateTo);
                    rkoCommand.Parameters.AddWithValue("@PARAM_PAGES", String.IsNullOrEmpty(metrology.CertPageCount) ? Convert.DBNull : metrology.CertPageCount);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся вставить RK_RKO_METROLOGY ");
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in rkoCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        rkoCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в  RKO_METROLOGY  c METROLOGY_ID = " + metrology.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в  RKO_METROLOGY  c METROLOGY_ID = " + metrology.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in rkoCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                conn.Close();
            }

            return 0;
        }

        public static int ReplicateAct(ActEntity a, DateTime maxDate)
        {
            string cmdText;

            if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
            {
                if (EntityFound("ACTS", a.Id, maxDate))
                {
                    //UPDATE
                }
                else
                {
                    //INSERT
                }
            }

            if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
            {
                if (EntityFound("ACTS", a.Id, maxDate))
                {
                    //UPDATE
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Существует акт с ID =" + a.Id);
                    }

                    cmdText = 
                        @"update rk_act_data 
                          set 
                              act_num = @param_act_num
                            , act_date = @param_act_date
                            , id_sp_activity = @param_id_sp_activity
                            , osnovanie = @param_osnovanie
                            , vyvod = @param_vyvod
                            , location = @param_location
                            , protocol_data_string = @param_protocol_data_string
                            , id_man_confirm = @param_id_man_confirm
                            , departcount = @param_departcount
                            , manhour =  @param_manhour
                            , id_rk_request_task = @param_id_rk_request_task
                            , dt_modify = @dt_modify 
                            , rpl_id_server = @param_rpl_id_server
                          where rk_act_Data.id_act_data = " + a.Id;
                }
                else
                {
                    //INSERT
                    if (_ClientSettings.Verbose) logger.Trace("Не существует акта с ID =" + a.Id);
                    cmdText = 
                        @"insert into rk_act_data 
                             (id_act_data
                            , act_num
                            , act_date
                            , id_sp_activity
                            , osnovanie
                            , vyvod
                            , location
                            , protocol_data_string
                            , id_man_confirm
                            , departcount
                            , manhour
                            , id_rk_request_task
                            , dt_modify
                            , rpl_id_server) 
                          values 
                             (@param_id_act_data
                            , @param_act_num
                            , @param_act_date
                            , @param_id_sp_activity
                            , @param_osnovanie
                            , @param_vyvod
                            , @param_location
                            , @param_protocol_data_string
                            , @param_id_man_confirm
                            , @param_departcount
                            , @param_manhour
                            , @param_id_rk_request_task
                            , @dt_modify
                            , @param_rpl_id_server)";
                }

                SqlConnection conn = new SqlConnection(_ClientSettings.ClientConnectionString);
                SqlCommand addCommand = new SqlCommand(cmdText, conn);
                addCommand.Parameters.Clear();
                addCommand.Parameters.AddWithValue("@param_id_act_data", a.Id);
                addCommand.Parameters.AddWithValue("@param_act_num", String.IsNullOrEmpty(a.Number)  ? Convert.DBNull : a.Number);
                addCommand.Parameters.AddWithValue("@param_act_date", a.Date);
                addCommand.Parameters.AddWithValue("@param_id_sp_activity", String.IsNullOrEmpty(a.IdActivity)  ? Convert.DBNull : a.IdActivity);
                addCommand.Parameters.AddWithValue("@param_osnovanie", String.IsNullOrEmpty(a.ConditionAsText)  ? Convert.DBNull : a.ConditionAsText);
                addCommand.Parameters.AddWithValue("@param_vyvod",  String.IsNullOrEmpty(a.Conclusion)  ? Convert.DBNull : a.Conclusion);
                addCommand.Parameters.AddWithValue("@param_location", String.IsNullOrEmpty(a.Location)  ? Convert.DBNull : a.Location);
                addCommand.Parameters.AddWithValue("@param_protocol_data_string",  String.IsNullOrEmpty(a.ProtocolsAsText)  ? Convert.DBNull : a.ProtocolsAsText);
                addCommand.Parameters.AddWithValue("@param_id_man_confirm", String.IsNullOrEmpty(a.IdConfirmer)  ? Convert.DBNull : a.IdConfirmer);
                addCommand.Parameters.AddWithValue("@param_departcount", a.DepartureCount);
                addCommand.Parameters.AddWithValue("@param_manhour",a.ManHours);
                addCommand.Parameters.AddWithValue("@param_id_rk_request_task",  String.IsNullOrEmpty(a.IdRequestTasks)  ? Convert.DBNull : a.IdRequestTasks);
                addCommand.Parameters.AddWithValue("@dt_modify", a.DtModify);
                addCommand.Parameters.AddWithValue("@param_rpl_id_server", String.IsNullOrEmpty(a.RplIdServer) ? "-1" : a.RplIdServer);
                conn.Open();

                try
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Пытаемся вставить акт с ID =" + a.Id);
                    }

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при сoхранении сущности ACT с ID = " + a.Id);
                    logger.Error(e.Message);

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                //delete from foreign key tables
                addCommand.Parameters.Clear();
                addCommand.CommandText = "Delete from rk_rel_act_protocol where id_act_data = " + a.Id;

                try
                {
                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при удалении записи ");
                    logger.Error(e.Message);
                }

                addCommand.CommandText = "Delete from rk_act_performer where id_act_data = " + a.Id;

                try
                {
                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при удалении записи ");
                    logger.Error(e.Message);
                }

                addCommand.CommandText = "Delete from RK_REL_ACT_RKO where id_act_data = " + a.Id;

                try
                {
                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при удалении записи ");
                    logger.Error(e.Message);
                }

                /*foreach (CloudActWorkTime workTime in a.WorkTimes)
                {
                    не используется
                    addCommand.CommandText = "insert into ";
                    addCommand.Parameters.Clear();
                    addCommand.ExecuteNonQuery();
                }*/
                /*foreach (CloudActErrorClass errorClass in a.ActErrorClasses)
                {не используется
                    addCommand.CommandText = "insert";
                    addCommand.Parameters.Clear();
                    addCommand.ExecuteNonQuery();
                }*/
                /*foreach (CloudActStatus actStatus in a.ActStatuses)
                { не используется
                    addCommand.CommandText = "insert";
                    addCommand.Parameters.Clear();
                    addCommand.ExecuteNonQuery();
                }*/
                /*foreach (CloudActRequestDetails requestDetail in a.ActRequestDetails)
                {не используется
                    addCommand.CommandText = "insert";
                    addCommand.Parameters.Clear();
                    addCommand.ExecuteNonQuery();
                }*/

                foreach (CloudRel protocol in a.Protocols)
                {
                    addCommand.CommandText =
                        @"insert into rk_rel_act_protocol 
                             (id_rel_act_protocol
                            , id_protocol, id_act_data
                            , rpl_id_server) 
                          values 
                             (@param_id_rel_act_protocol
                            , @param_id_protocol
                            , @param_id_act_data
                            , @param_rpl_id_server)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@param_id_rel_act_protocol", String.IsNullOrEmpty(protocol.Id)  ? Convert.DBNull : protocol.Id);
                    addCommand.Parameters.AddWithValue("@param_id_protocol", String.IsNullOrEmpty(protocol.ForeignKeyId)  ? Convert.DBNull : protocol.ForeignKeyId);
                    addCommand.Parameters.AddWithValue("@param_id_act_data", a.Id);
                    addCommand.Parameters.AddWithValue("@Param_rpl_id_server", String.IsNullOrEmpty(protocol.RplIdServer)  ? "-1" : protocol.RplIdServer);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся вставить RK_REL_ACT_PROTOCOL с ID_ACT_DATA = " + a.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в  rk_rel_act_protocol with ACT_ID = " + a.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в  rk_rel_act_protocol with ACT_ID = " + a.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                foreach (CloudRel performer in a.ActPerformers)
                {
                    addCommand.CommandText =
                        @"insert into rk_act_performer 
                             (ID_ACT_PERFORMER
                            , ID_SOTRUDNIK
                            , ID_ACT_DATA
                            , rpl_id_server) 
                          values 
                             (@param_id_act_perfomer
                            , @param_id_sotrudnik
                            , @param_id_act_data
                            , @param_rpl_id_server)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@param_id_act_perfomer", String.IsNullOrEmpty(performer.Id)  ? Convert.DBNull : performer.Id);
                    addCommand.Parameters.AddWithValue("@param_id_sotrudnik", String.IsNullOrEmpty(performer.ForeignKeyId)  ? Convert.DBNull : performer.ForeignKeyId);
                    addCommand.Parameters.AddWithValue("@param_id_act_data", a.Id);
                    addCommand.Parameters.AddWithValue("@param_rpl_id_server",  String.IsNullOrEmpty(performer.RplIdServer)  ? "-1" : performer.RplIdServer);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся вставить RK_ACT_PERFORMER с ID_ACT_DATA = " + a.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в rk_act_performer with ACT_ID = " + a.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в rk_act_performer with ACT_ID = " + a.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                foreach (CloudRel rko in a.RkoList)
                {
                    addCommand.CommandText = 
                        @"insert into RK_REL_ACT_RKO 
                             (ID_RKO
                            , ID_ACT_DATA
                            , RPL_ID_SERVER) 
                          values 
                             (@param_id_rko
                            , @param_id_act_data
                            , @param_rpl_id_server)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@param_id_rko", String.IsNullOrEmpty(rko.ForeignKeyId)  ? Convert.DBNull : rko.ForeignKeyId);
                    addCommand.Parameters.AddWithValue("@param_id_act_data", a.Id);
                    addCommand.Parameters.AddWithValue("@param_rpl_id_server",
                        String.IsNullOrEmpty(rko.RplIdServer) ? "-1" : rko.RplIdServer);
                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся вставить RK_REL_ACT_RKO с ID_ACT_DATA = " + a.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в RK_REL_ACT_RKO with ACT_ID = " + a.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в RK_REL_ACT_RKO with ACT_ID = " + a.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                /*foreach (CloudRel task in a.Tasks)
                {не используется
                    addCommand.CommandText =
                        " " +
                        "  ";
                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@param_id_rel_act_rko", task.Id);
                    addCommand.Parameters.AddWithValue("@param_id_rko", task.ForeignKeyId);
                    addCommand.Parameters.AddWithValue("@param_id_act_data", a.Id);
                    addCommand.ExecuteNonQuery();
                }*/
                /* actAuthors тоже не используется в ms sql*/

                conn.Close();
            }

            return 0;
        }

        public static int ReplicateProtocol(ProtocolEntity p, DateTime maxDate)
        {
            //сущность с данным адйишником пытается вставить в базу данных
                //вставляем сущность протокола
            //проверим есть ли протокол с тем же ИД и с dt_modify < чем она пришла с сервера


            //проверим есть ли rk_request
            //проверим есть ли rk_request_task
            string cmdText = "";

            #region firebird
            if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
            {
                if (EntityFound("PROTOCOLS", p.Id.ToString(), maxDate))
                {
                    cmdText = "UPDATE ";
                }
                else
                {
                    cmdText = 
                        @"insert into RES_PROTOCOL_IZM_TP 
                             (id_protocol
                            , rpl_id_server
                            , num_protocol
                            , date_protocol
                            /*, id_client */ )" +
                        //" id_res,  id_metod," +
                        //" description, zadanie_num, zadanie_date, zayavka_nomer," +
                        //" zayavka_date, " +
                        //" id_sr_izm, id_rk_equipmentset," +
                        //" id_protocol_type, numattach, id_rk_type_narush," +
                        //" id_measur_condition, departcount, manhour,  pagecount," +
                        //" id_rk_request_task,  id_client_dbs," +
                        //" id_res_dbs,  id_client_eis, id_res_eis," +
                        //" id_rk_request_res, id_rk_request_frq_zone, id_rk_request_freq, id_rk_request_rec," +
                        //" id_rk_request_sanct) " +
                        @"values 
                             (@param_id_protocol
                            , 1
                            , @param_num_protocol
                            , @param_date_protocol
                            , @param_zadanie_date
                            , @param_zayavka_date
                            /*, @param_id_client */ )";
                        //" @param_id_res,  @param_id_metod," +
                        //" @param_description, @param_zadanie_num, @param_zadanie_date, @param_zayavka_nomer," +
                        //" @param_zayavka_date, " +
                        //" @param_id_sr_izm, @param_id_rk_equipmentset," +
                        //" @param_id_protocol_type, @param_numattach, @param_id_rk_type_narush," +
                        //" @param_id_measur_condition, @param_departcount, @param_manhour,  @param_pagecount," +
                        //" @param_id_rk_request_task,  @param_id_client_dbs," +
                        //" @param_id_res_dbs,  @param_id_client_eis, @param_id_res_eis," +
                        //" @param_id_rk_request_res, @param_id_rk_request_frq_zone, @param_id_rk_request_freq, @param_id_rk_request_rec," +
                        //" @param_id_rk_request_sanct)";
                }

                FbConnection fbconn = new FbConnection(_ClientSettings.ClientConnectionString);
                fbconn.Open();
                FbTransaction fbtran = fbconn.BeginTransaction();
                FbCommand addCommand = new FbCommand(cmdText, fbconn);
                addCommand.Connection = fbconn;
                addCommand.Transaction = fbtran;

                addCommand.Parameters.AddWithValue("param_id_protocol", p.Id);
                addCommand.Parameters.AddWithValue("param_num_protocol", p.Number);
                //addCommand.Parameters.AddWithValue("param_date_protocol", p.Date.Year.ToString()+"-"+p.Date.Month.ToString()+"-"+p.Date.Day.ToString());
                //addCommand.Parameters.AddWithValue("param_date_protocol", p.Date.Day.ToString() + "." + p.Date.Month.ToString() + "." + p.Date.Year.ToString());
                addCommand.Parameters.AddWithValue("param_date_protocol", p.Date.ToShortDateString());
                //addCommand.Parameters.AddWithValue("param_id_client", p.IdClient);
                //addCommand.Parameters.AddWithValue("param_id_res", p.IdRes);
                //addCommand.Parameters.AddWithValue("param_id_metod", p.IdMethodic);
                //addCommand.Parameters.AddWithValue("param_description", p.Description);
                //addCommand.Parameters.AddWithValue("param_zadanie_num", p.TaskNumber);
                addCommand.Parameters.AddWithValue("param_zadanie_date", p.TaskDate.ToShortDateString());
                //addCommand.Parameters.AddWithValue("param_zayavka_nomer", p.IssueNumber);
                addCommand.Parameters.AddWithValue("param_zayavka_date", p.IssueDate.ToShortDateString());
                //addCommand.Parameters.AddWithValue("param_id_sr_izm", p.IdRko);
                //addCommand.Parameters.AddWithValue("param_id_rk_equipmentset", p.IdEquipmentset);
                //addCommand.Parameters.AddWithValue("param_id_protocol_type", p.IdProtocolType);
                //addCommand.Parameters.AddWithValue("param_numattach", p.AttachmentCount);
                //addCommand.Parameters.AddWithValue("param_id_rk_type_narush", p.IdViolationType);
                //addCommand.Parameters.AddWithValue("param_id_measur_condition", p.IdMeasureCondition);
                //addCommand.Parameters.AddWithValue("param_departcount", p.DepartureCount);
                //addCommand.Parameters.AddWithValue("param_manhour", p.ManHours);
                //addCommand.Parameters.AddWithValue("param_pagecount", p.PageCount);
                //addCommand.Parameters.AddWithValue("param_id_rk_request_task", p.IdRequestTask);
                //addCommand.Parameters.AddWithValue("param_id_client_dbs", p.IdClientDBS);
                //addCommand.Parameters.AddWithValue("param_id_res_dbs", p.IdResDBS);
                //addCommand.Parameters.AddWithValue("param_id_client_eis", p.IdClientEIS);
                //addCommand.Parameters.AddWithValue("param_id_res_eis", p.IdResEIS);
                //addCommand.Parameters.AddWithValue("param_id_rk_request_res", p.IdRequestRes);
                //addCommand.Parameters.AddWithValue("param_id_rk_request_frq_zone", p.IdRequestFreqZone);
                //addCommand.Parameters.AddWithValue("param_id_rk_request_freq", p.IdRequestFreq);
                //addCommand.Parameters.AddWithValue("param_id_rk_request_rec", p.IdRequestRec);
                //addCommand.Parameters.AddWithValue("param_id_rk_request_sanct", p.IdRequestSanct);
                ////
                //var logfile = new System.IO.StreamWriter("log.txt");
                //logfile.WriteLine(addCommand.CommandText);
                //logfile.Close();

                string query = addCommand.CommandText;

                foreach (FbParameter pp in addCommand.Parameters)
                {
                    query = query.Replace(pp.ParameterName, pp.Value.ToString());
                }   
                
                //Log.Write(query);
                ////
                addCommand.ExecuteNonQuery();
                fbtran.Commit();
                addCommand.Dispose();
                fbconn.Close();
            }
            #endregion firebird

            if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
            {
                if (EntityFound("PROTOCOLS", p.Id.ToString(), maxDate))
                {
                    if (_ClientSettings.Verbose) logger.Trace("Найдена сущность ПРОТОКОЛ с ID = " + p.Id.ToString());

                    cmdText = 
                        @"UPDATE RK_PROTOCOLS 
                          set 
                              num_protocol = @param_num_protocol
                            , date_protocol = @param_date_protocol
                            , id_client = @param_id_client
                            , id_res = @param_id_res
                            , id_metod = @param_id_metod
                            , description = @param_description
                            , zadanie_num = @param_zadanie_num
                            , zadanie_date = @param_zadanie_date
                            , zayavka_nomer = @param_zayavka_nomer
                            , zayavka_date = @param_zayavka_date
                            , id_sr_izm = @param_id_sr_izm
                            , id_rk_equipmentset = @param_id_rk_equipmentset
                            , id_protocol_type = @param_id_protocol_type
                            , numattach = @param_numattach
                            , id_rk_type_narush = @param_id_rk_type_narush
                            , id_measur_condition = @param_id_measur_condition
                            , departcount = @param_departcount
                            , manhour = @param_manhour
                            , pagecount = @param_pagecount
                            , id_rk_request_task = @param_id_rk_request_task
                            , id_client_dbs = @param_id_client_dbs
                            , id_res_dbs = @param_id_res_dbs
                            , id_client_eis = @param_id_client_eis
                            , id_res_eis = @param_id_res_eis
                            , dt_modify = @dt_modify
                            , rpl_id_server = @param_rpl_id_server 
                          where RK_PROTOCOLS.id_protocol = " + p.Id;
                }
                else
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Не найдена сущность ПРОТОКОЛ с ID = " + p.Id.ToString());
                    }

                    cmdText = 
                        @"insert into RK_PROTOCOLS 
                             (id_protocol
                            , num_protocol
                            , date_protocol
                            , id_client
                            , id_res
                            , id_metod
                            , description
                            , zadanie_num
                            , zadanie_date
                            , zayavka_nomer
                            , zayavka_date
                            , id_sr_izm
                            , id_rk_equipmentset
                            , id_protocol_type
                            , numattach
                            , id_rk_type_narush
                            , id_measur_condition
                            , departcount
                            , manhour
                            , pagecount
                            , id_rk_request_task
                            , id_client_dbs
                            , id_res_dbs
                            , id_client_eis
                            , id_res_eis
                            , dt_modify
                            , rpl_id_server)
                          values 
                             (@param_id_protocol
                            , @param_num_protocol
                            , @param_date_protocol
                            , @param_id_client
                            , @param_id_res
                            , @param_id_metod
                            , @param_description
                            , @param_zadanie_num
                            , @param_zadanie_date
                            , @param_zayavka_nomer
                            , @param_zayavka_date
                            , @param_id_sr_izm
                            , @param_id_rk_equipmentset
                            , @param_id_protocol_type
                            , @param_numattach
                            , @param_id_rk_type_narush
                            , @param_id_measur_condition
                            , @param_departcount
                            , @param_manhour
                            , @param_pagecount
                            , @param_id_rk_request_task
                            , @param_id_client_dbs
                            , @param_id_res_dbs
                            , @param_id_client_eis
                            , @param_id_res_eis
                            , @dt_modify
                            , @param_rpl_id_server)";
                }

                SqlConnection conn = new SqlConnection(_ClientSettings.ClientConnectionString);
                SqlCommand addCommand = new SqlCommand(cmdText, conn);
                addCommand.Parameters.AddWithValue("@param_id_protocol", p.Id);
                addCommand.Parameters.AddWithValue("@param_num_protocol", String.IsNullOrEmpty(p.Number) ? Convert.DBNull : p.Number);
                addCommand.Parameters.AddWithValue("@param_date_protocol", p.Date);
                addCommand.Parameters.AddWithValue("@param_id_client", String.IsNullOrEmpty(p.IdClient) ? Convert.DBNull : p.IdClient);
                addCommand.Parameters.AddWithValue("@param_id_res", String.IsNullOrEmpty(p.IdRes) ? Convert.DBNull : p.IdRes);
                addCommand.Parameters.AddWithValue("@param_id_metod", String.IsNullOrEmpty(p.IdMethodic) ? Convert.DBNull : p.IdMethodic);
                addCommand.Parameters.AddWithValue("@param_description", String.IsNullOrEmpty(p.Description) ? Convert.DBNull : p.Description);
                addCommand.Parameters.AddWithValue("@param_zadanie_num", String.IsNullOrEmpty(p.TaskNumber) ? Convert.DBNull : p.TaskNumber);
                addCommand.Parameters.AddWithValue("@param_zadanie_date", p.TaskDate);
                addCommand.Parameters.AddWithValue("@param_zayavka_nomer", String.IsNullOrEmpty(p.IssueNumber) ? Convert.DBNull : p.IssueNumber);
                addCommand.Parameters.AddWithValue("@param_zayavka_date", p.IssueDate);
                addCommand.Parameters.AddWithValue("@param_id_sr_izm", String.IsNullOrEmpty(p.IdRko) ? Convert.DBNull : p.IdRko);
                addCommand.Parameters.AddWithValue("@param_id_rk_equipmentset", String.IsNullOrEmpty(p.IdEquipmentset) ? Convert.DBNull : p.IdEquipmentset);
                addCommand.Parameters.AddWithValue("@param_id_protocol_type", String.IsNullOrEmpty(p.IdProtocolType) ? Convert.DBNull : p.IdProtocolType);
                addCommand.Parameters.AddWithValue("@param_numattach", String.IsNullOrEmpty(p.AttachmentCount) ? Convert.DBNull : p.AttachmentCount);
                addCommand.Parameters.AddWithValue("@param_id_rk_type_narush", String.IsNullOrEmpty(p.IdViolationType) ? Convert.DBNull : p.IdViolationType);
                addCommand.Parameters.AddWithValue("@param_id_measur_condition", String.IsNullOrEmpty(p.IdMeasureCondition) ? Convert.DBNull : p.IdMeasureCondition);
                addCommand.Parameters.AddWithValue("@param_departcount", String.IsNullOrEmpty(p.DepartureCount) ? Convert.DBNull : p.DepartureCount);
                addCommand.Parameters.AddWithValue("@param_manhour", String.IsNullOrEmpty(p.ManHours) ? Convert.DBNull : p.ManHours.Replace(",","."));
                addCommand.Parameters.AddWithValue("@param_pagecount", String.IsNullOrEmpty(p.PageCount) ? Convert.DBNull : p.PageCount);
                addCommand.Parameters.AddWithValue("@param_id_rk_request_task", String.IsNullOrEmpty(p.IdRequestTask) ? Convert.DBNull : p.IdRequestTask);
                addCommand.Parameters.AddWithValue("@param_id_client_dbs", String.IsNullOrEmpty(p.IdClientDBS) ? Convert.DBNull : p.IdClientDBS);
                addCommand.Parameters.AddWithValue("@param_id_res_dbs", String.IsNullOrEmpty(p.IdResDBS) ? Convert.DBNull : p.IdResDBS);
                addCommand.Parameters.AddWithValue("@param_id_client_eis", String.IsNullOrEmpty(p.IdClientEIS) ? Convert.DBNull : p.IdClientEIS);
                addCommand.Parameters.AddWithValue("@param_id_res_eis", String.IsNullOrEmpty(p.IdResEIS) ? Convert.DBNull : p.IdResEIS);
                addCommand.Parameters.AddWithValue("@dt_modify", p.DtModify);
                addCommand.Parameters.AddWithValue("@param_rpl_id_server", String.IsNullOrEmpty(p.RplIdServer) ? Convert.DBNull : p.RplIdServer);

                ////
                //var logfile = new System.IO.StreamWriter("log.txt");
                //logfile.WriteLine(addCommand.CommandText);
                //logfile.Close();
                //string query = addCommand.CommandText;
                //foreach (SqlParameter pp in addCommand.Parameters)
                //{
                //    query = query.Replace(pp.ParameterName, pp.Value.ToString());
                //}

                //Log.Write(query);
                ////

                conn.Open();

                try
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Пытаемся сохранить RK_PROTOCOLS с ID = " + p.Id);
                    }

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при сохранении сущности PROTOCOL с ID = " + p.Id.ToString());
                    logger.Error(e.Message);
                    //Log.Write("Произошла ошибка при сохранении сущности PROTOCOL с ID = " + p.Id.ToString());
                    //Log.Write(e.Message);
                    
                    foreach (SqlParameter param in addCommand.Parameters)
                    {
                        logger.Error(param.ParameterName + " = " + param.Value.ToString());
                    }
                }

                //вставляет дочерние ветки записей в зависимые таблицы
                //предварительно удалив то, что было
                try
                {
                    addCommand.Parameters.Clear();
                    addCommand.CommandText = "Delete from RK_PRTCL_FRQ where id_protocol = " + p.Id.ToString();
                    addCommand.ExecuteNonQuery();
                    addCommand.CommandText = "Delete from RK_PRTCL_BAND where id_protocol = " + p.Id.ToString();
                    addCommand.ExecuteNonQuery();
                    addCommand.CommandText = "Delete from RK_PRTCL_EMISS where id_protocol = " + p.Id.ToString();
                    addCommand.ExecuteNonQuery();
                    addCommand.CommandText = "Delete from RK_PRTCL_EMP where id_protocol = " + p.Id.ToString();
                    addCommand.ExecuteNonQuery();
                    addCommand.CommandText = "Delete from RK_PRTCL_RES where id_protocol = " + p.Id.ToString();
                    addCommand.ExecuteNonQuery();
                    addCommand.CommandText = "Delete from RK_PRTCL_STRENGTH where id_protocol = " + p.Id.ToString();
                    addCommand.ExecuteNonQuery();
                    addCommand.CommandText = "Delete from RK_PRTCL_SOTRUDNIK where id_protocol = " + p.Id.ToString();
                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при удалении данных из дочерних таблиц при вставке протокола с ID = " + p.Id);
                    logger.Error(e.Message);
                }

                foreach (CloudProtocolFreq freq in p.Freq)
                {
                    //"select PF.ID_RK_PRTCL_FRQ, PF.FRQ_ALLOW_MIN, PF.FRQ_ALLOW_MAX, PF.FRQ_MEASURED, PF.FRQ_MEASURED_FROM, PF.FRQ_MEASURED_TO" +
                    //", PF.FRQ_PERMITT, PF.FRQ_PERMITT_FROM, PF.FRQ_PERMITT_TO, PF.FRQ_POGR, PF.FREQTYPE, PF.ID_RK_RAZMERNOST, PF.IS_NORMALLY, PF.IS_EXISTS " +
                    //"from RK_PRTCL_FRQ PF " +
                    //"where PF.ID_PROTOCOL = " + entityId;

                    addCommand.CommandText = 
                        @"insert into RK_PRTCL_FRQ 
                             (ID_PROTOCOL
                            , RPL_ID_SERVER
                            , ID_RK_PRTCL_FRQ
                            , FRQ_ALLOW_MIN
                            , FRQ_ALLOW_MAX
                            , FRQ_MEASURED
                            , FRQ_MEASURED_FROM
                            , FRQ_MEASURED_TO
                            , FRQ_PERMITT
                            , FRQ_PERMITT_FROM
                            , FRQ_PERMITT_TO
                            , FRQ_POGR
                            , ID_RK_RAZMERNOST
                            , IS_NORMALLY
                            , IS_EXISTS
                            , RPL_ID_SERVER) 
                          VALUES 
                             (@PARAM_ID_PROTOCOL
                            , @PARAM_RPL_ID_SERVER
                            , @PARAM_ID_RK_PRTCL_FRQ
                            , @PARAM_FRQ_ALLOW_MIN
                            , @PARAM_FRQ_ALLOW_MAX
                            , @PARAM_FRQ_MEASURED
                            , @PARAM_FRQ_MEASURED_FROM
                            , @PARAM_FRQ_MEASURED_TO
                            , @PARAM_FRQ_PERMITT
                            , @PARAM_FRQ_PERMITT_FROM
                            , @PARAM_FRQ_PERMITT_TO
                            , @PARAM_FRQ_POGR
                            , @PARAM_ID_RK_RAZMERNOST
                            , @PARAM_IS_NORMALLY
                            , @PARAM_IS_EXISTS
                            , @PARAM_RPL_ID_SERVER)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_ID_PROTOCOL", p.Id);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", -1);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PRTCL_FRQ", (String.IsNullOrEmpty(freq.Id) ? Convert.DBNull : freq.Id));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_ALLOW_MIN", (String.IsNullOrEmpty(freq.FreqAllowMin) ? Convert.DBNull : freq.FreqAllowMin.Replace(",", ".")));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_ALLOW_MAX", (String.IsNullOrEmpty(freq.FreqAllowMax) ? Convert.DBNull : freq.FreqAllowMax.Replace(",", ".")));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_MEASURED", (String.IsNullOrEmpty(freq.MeasuredFreq) ? Convert.DBNull : freq.MeasuredFreq.Replace(",", ".")));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_PERMITT", (String.IsNullOrEmpty(freq.PermitFreq) ? Convert.DBNull : freq.PermitFreq.Replace(",", ".")));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_PERMITT_FROM", (String.IsNullOrEmpty(freq.FreqPermitFrom) ? Convert.DBNull : freq.FreqPermitFrom.Replace(",", ".")));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_PERMITT_TO", (String.IsNullOrEmpty(freq.FreqPermitTo) ? Convert.DBNull : freq.FreqPermitTo.Replace(",", ".")));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_POGR", (String.IsNullOrEmpty(freq.FreqDelta) ? Convert.DBNull : freq.FreqDelta.Replace(",", ".")));
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_RAZMERNOST", (String.IsNullOrEmpty(freq.IdMeasureLevel) ? "0" : freq.IdMeasureLevel));
                    addCommand.Parameters.AddWithValue("@PARAM_IS_NORMALLY", Convert.ToInt16(freq.IsValid));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_MEASURED_FROM", (String.IsNullOrEmpty(freq.FreqMeasuredFrom) ? Convert.DBNull : freq.FreqMeasuredFrom.Replace(",", ".")));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_MEASURED_TO", (String.IsNullOrEmpty(freq.FreqMeasuredTo) ? Convert.DBNull : freq.FreqMeasuredTo.Replace(",", ".")));
                    addCommand.Parameters.AddWithValue("@PARAM_IS_EXISTS", Convert.ToInt16(freq.IsExists));
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER",String.IsNullOrEmpty(freq.RplIdServer) ? "-1" : freq.RplIdServer);
                    
                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся сохранить RK_PRTCL_FRQ с ID_PROTOCOL = " + p.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке в RK_PRTCL_FRQ с ID_PROTOCOL = " + p.Id);
                        logger.Error(e.Message);

                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                foreach (CloudProtocolBand band in p.Band)
                {
                    if (band.Id != null)
                    {
                        addCommand.CommandText = 
                            @"insert into RK_PRTCL_BAND 
                                 (ID_PROTOCOL
                                , RPL_ID_SERVER
                                , ID_RK_PRTCL_BAND
                                , FRQ_PERMITT
                                , BAND_MEASURED
                                , BAND_ALLOW
                                , BAND_POGR 
                                , ID_RK_RAZMERNOST
                                , IS_NORMALLY
                                , ID_PRTCL_FRQ
                                , RPL_ID_SERVER) 
                              VALUES 
                                 (@PARAM_ID_PROTOCOL
                                , @PARAM_RPL_ID_SERVER
                                , @PARAM_ID_RK_PRTCL_BAND
                                , @PARAM_FRQ_PERMITT
                                , @PARAM_BAND_MEASURED
                                , @PARAM_BAND_ALLOW
                                , @PARAM_BAND_POGR 
                                , @PARAM_ID_RK_RAZMERNOST
                                , @PARAM_IS_NORMALLY
                                , @PARAM_ID_PRTCL_FRQ
                                , @PARAM_RPL_ID_SERVER)";

                        addCommand.Parameters.Clear();
                        addCommand.Parameters.AddWithValue("@PARAM_ID_PROTOCOL", p.Id.ToString());
                        addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", -1);
                        addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PRTCL_BAND", (String.IsNullOrEmpty(band.Id)) ? Convert.DBNull : band.Id);
                        addCommand.Parameters.AddWithValue("@PARAM_FRQ_PERMITT",
                            (String.IsNullOrEmpty(band.PermitFreq)) ? Convert.DBNull : band.PermitFreq.Replace(",","."));
                        addCommand.Parameters.AddWithValue("@PARAM_BAND_MEASURED",
                            (String.IsNullOrEmpty(band.MeasuredBand)) ? Convert.DBNull : band.MeasuredBand.Replace(",","."));
                        addCommand.Parameters.AddWithValue("@PARAM_BAND_ALLOW",
                            (String.IsNullOrEmpty(band.BandAllowed)) ? Convert.DBNull : band.BandAllowed.Replace(",", "."));
                        addCommand.Parameters.AddWithValue("@PARAM_BAND_POGR",
                            (String.IsNullOrEmpty(band.BandDelta)) ? Convert.DBNull : band.BandDelta.Replace(",", "."));
                        addCommand.Parameters.AddWithValue("@PARAM_ID_RK_RAZMERNOST",
                            (String.IsNullOrEmpty(band.IdMeasureLevel)) ? "0" : band.IdMeasureLevel);
                        addCommand.Parameters.AddWithValue("@PARAM_IS_NORMALLY", Convert.ToInt16(band.IsValid));
                        addCommand.Parameters.AddWithValue("@PARAM_ID_PRTCL_FRQ",
                            (String.IsNullOrEmpty(band.IdFreq)) ? Convert.DBNull : band.IdFreq);
                        addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER",
                            (String.IsNullOrEmpty(band.RplIdServer)) ? "-1" : band.RplIdServer);
                       
                        try
                        {
                            if (_ClientSettings.Verbose)
                            {
                                logger.Trace("Пытаемся сохранить RK_PRTCL_BAND с ID_PROTOCOL = " + p.Id);
                            }

                            if (_ClientSettings.Verbose)
                            {
                                foreach (SqlParameter param in addCommand.Parameters)
                                {
                                    logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                                }
                            }

                            addCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            logger.Error("Произошла ошибка при вставке записи в RK_PRTCL_BAND с PROTOCOL_ID = " + p.Id);
                            logger.Error(e.Message);
                            //Log.Write("Произошла ошибка при вставке записи в RK_PRTCL_BAND с PROTOCOL_ID = " + p.Id);
                            //Log.Write(e.Message);

                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Error(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }
                    }
                }

                foreach (CloudProtocolEmiss emiss in p.Emiss)
                {
                    //"select PE.ID_RK_PRTCL_EMISS, PE.LEVEL_EM, PE.BAND_EM, PE.BAND_EM_ALLOW, PE.BAND_EM_POGR, PE.IS_NORMALLY" +
                    //" from RK_PRTCL_EMISS PE" +
                    //" where PE.ID_PROTOCOL = " + entityId;

                    addCommand.CommandText = 
                        @"insert into RK_PRTCL_EMISS 
                             (ID_PROTOCOL
                            , RPL_ID_SERVER
                            , ID_RK_PRTCL_EMISS
                            , LEVEL_EM
                            , BAND_EM
                            , BAND_EM_ALLOW
                            , BAND_EM_POGR
                            , IS_NORMALLY
                            , RPL_ID_SERVER) 
                          VALUES 
                            (@PARAM_ID_PROTOCOL
                            , @PARAM_RPL_ID_SERVER
                            , @PARAM_ID_RK_PRTCL_EMISS
                            , @PARAM_LEVEL_EM
                            , @PARAM_BAND_EM
                            , @PARAM_BAND_EM_ALLOW
                            , @PARAM_BAND_EM_POGR
                            , @PARAM_IS_NORMALLY
                            , @PARAM_RPL_ID_SERVER)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_ID_PROTOCOL", p.Id);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", -1);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PRTCL_EMISS", (String.IsNullOrEmpty(emiss.Id) ? Convert.DBNull : emiss.Id));
                    addCommand.Parameters.AddWithValue("@PARAM_LEVEL_EM", emiss.EmissLevel );
                    addCommand.Parameters.AddWithValue("@PARAM_BAND_EM", (String.IsNullOrEmpty(emiss.EmissBand) ? Convert.DBNull : emiss.EmissBand));
                    addCommand.Parameters.AddWithValue("@PARAM_BAND_EM_ALLOW", (String.IsNullOrEmpty(emiss.EmissBandAllow) ? Convert.DBNull : emiss.EmissBandAllow));
                    addCommand.Parameters.AddWithValue("@PARAM_BAND_EM_POGR", (String.IsNullOrEmpty(emiss.EmissBandDelta) ? Convert.DBNull : emiss.EmissBandDelta));
                    addCommand.Parameters.AddWithValue("@PARAM_IS_NORMALLY", Convert.ToInt16(emiss.IsValid));
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER",
                        String.IsNullOrEmpty(emiss.RplIdServer) ? "-1" : emiss.RplIdServer);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся сохранить RK_PRTCL_EMISS с ID_PROTOCOL = " + p.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в RK_PRTCL_EMISS с PROTOCOL_ID = " + p.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в RK_PRTCL_EMISS с PROTOCOL_ID = " + p.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                foreach (CloudProtocolEmp emp in p.Emp)
                {
                    //"select PE.ID_RK_PRTCL_EMP, PE.MEASURE_DATE, PE.MEASURE_AZIMUTH, PE.MEASURE_DISTANCE, PE.MEASURE_GEO_LAT, PE.MEASURE_GEO_LONG, PE.MEASURE_LOCATION" +
                    //" from RK_PRTCL_EMP PE" +
                    //" where PE.ID_PROTOCOL = " + entityId;

                    addCommand.CommandText = 
                        @"insert into RK_PRTCL_EMP 
                             (ID_PROTOCOL
                            , RPL_ID_SERVER
                            , ID_RK_PRTCL_EMP
                            , MEASURE_DATE
                            , MEASURE_AZIMUTH
                            , MEASURE_DISTANCE
                            , MEASURE_GEO_LAT
                            , MEASURE_GEO_LONG
                            , MEASURE_LOCATION
                            , RPL_ID_SERVER) 
                          VALUES 
                             (@PARAM_ID_PROTOCOL
                            , @PARAM_RPL_ID_SERVER
                            , @PARAM_ID_RK_PRTCL_EMP
                            , @PARAM_MEASURE_DATE
                            , @PARAM_MEASURE_AZIMUTH
                            , @PARAM_MEASURE_DISTANCE
                            , @PARAM_MEASURE_GEO_LAT
                            , @PARAM_MEASURE_GEO_LONG
                            , @PARAM_MEASURE_LOCATION
                            , @PARAM_RPL_ID_SERVER)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_ID_PROTOCOL", p.Id);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", -1);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PRTCL_EMP", (String.IsNullOrEmpty(emp.Id) ? Convert.DBNull : emp.Id));
                    addCommand.Parameters.AddWithValue("@PARAM_MEASURE_DATE", emp.MeasureDate);
                    addCommand.Parameters.AddWithValue("@PARAM_MEASURE_AZIMUTH", (String.IsNullOrEmpty(emp.MeasureAzimuth) ? Convert.DBNull : emp.MeasureAzimuth));
                    addCommand.Parameters.AddWithValue("@PARAM_MEASURE_DISTANCE", (String.IsNullOrEmpty(emp.MeasureDistance) ? Convert.DBNull : emp.MeasureDistance));
                    addCommand.Parameters.AddWithValue("@PARAM_MEASURE_GEO_LAT", (String.IsNullOrEmpty(emp.MeasureGeoLat) ? Convert.DBNull : emp.MeasureGeoLat));
                    addCommand.Parameters.AddWithValue("@PARAM_MEASURE_GEO_LONG", (String.IsNullOrEmpty(emp.MeasureGeoLong) ? Convert.DBNull : emp.MeasureGeoLong));
                    addCommand.Parameters.AddWithValue("@PARAM_MEASURE_LOCATION", (String.IsNullOrEmpty(emp.MeasureLocation) ? Convert.DBNull : emp.MeasureLocation));
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(emp.RplIdServer) ? "-1" : emp.RplIdServer);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся сохранить RK_PRTCL_EMP с ID_PROTOCOL = " + p.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в RK_PRTCL_EMP с PROTOCOL_ID = " + p.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в RK_PRTCL_EMP с PROTOCOL_ID = " + p.Id);
                        //Log.Write(e.Message);
                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                foreach (CloudProtocolStrength strength in p.Strength)
                {
                    //"select PS.ID_RK_PRTCL_STRENGTH, PS.STRENGTH, PS.STRENGTH_NORM, PS.FRQ_MEAS_MIN, PS.FRQ_MEAS_MAX, PS.ID_RK_RAZMERNOST," +
                    //" PS.IS_NORMALLY" +
                    //" from  RK_PRTCL_STRENGTH PS" +
                    //" where PS.ID_PROTOCOL = " + entityId;

                    addCommand.CommandText = 
                        @"insert into RK_PRTCL_STRENGTH 
                             (ID_PROTOCOL
                            , RPL_ID_SERVER
                            , ID_RK_PRTCL_STRENGTH
                            , STRENGTH
                            , STRENGTH_NORM
                            , FRQ_MEAS_MIN
                            , FRQ_MEAS_MAX
                            , ID_RK_RAZMERNOST
                            ,IS_NORMALLY
                            , RPL_ID_SERVER) 
                          VALUES 
                             (@PARAM_ID_PROTOCOL
                            , @PARAM_RPL_ID_SERVER
                            , @PARAM_ID_RK_PRTCL_STRENGTH
                            , @PARAM_STRENGTH
                            , @PARAM_STRENGTH_NORM
                            , @PARAM_FRQ_MEAS_MIN
                            , @PARAM_FRQ_MEAS_MAX
                            , @PARAM_ID_RK_RAZMERNOST
                            , @PARAM_IS_NORMALLY
                            , @PARAM_RPL_ID_SERVER)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_ID_PROTOCOL", p.Id);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", -1);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PRTCL_STRENGTH",  (String.IsNullOrEmpty(strength.Id) ? Convert.DBNull : strength.Id));
                    addCommand.Parameters.AddWithValue("@PARAM_STRENGTH",  (String.IsNullOrEmpty(strength.Strength) ? Convert.DBNull : strength.Strength));
                    addCommand.Parameters.AddWithValue("@PARAM_STRENGTH_NORM",  (String.IsNullOrEmpty(strength.StrengthNormal) ? Convert.DBNull : strength.StrengthNormal));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_MEAS_MIN",  (String.IsNullOrEmpty(strength.FreqMeasureMin) ? Convert.DBNull : strength.FreqMeasureMin));
                    addCommand.Parameters.AddWithValue("@PARAM_FRQ_MEAS_MAX",  (String.IsNullOrEmpty(strength.FreqMeasureMax) ? Convert.DBNull : strength.FreqMeasureMax));
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_RAZMERNOST", (String.IsNullOrEmpty(strength.IdMeasureLevel) ? "0" : strength.IdMeasureLevel));
                    addCommand.Parameters.AddWithValue("@PARAM_IS_NORMALLY", Convert.ToInt16(strength.IsValid));
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER",
                        String.IsNullOrEmpty(strength.RplIdServer) ? "-1" : strength.RplIdServer);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся сохранить RK_PRTCL_STRENGTH с ID_PROTOCOL = " + p.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в RK_PRTCL_STRENGTH с PROTOCOL_ID = " + p.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в RK_PRTCL_STRENGTH с PROTOCOL_ID = " + p.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }
                //protocolauthors
                //"select S.id_rk_prtcl_sotrudnik from rk_prtcl_sotrudnik S where S.id_protocol = " + entityId;

                foreach (CLoudProtocolAuthors sotrudnik in p.ProtocolAuthors)
                {
                    addCommand.CommandText = 
                        @"insert into RK_PRTCL_SOTRUDNIK 
                             (id_protocol
                            , id_rk_prtcl_sotrudnik
                            , id_sotrudnik
                            , rpl_id_server)
                          VALUES 
                            (@param_id_protocol
                            , @param_id_rk_prtcl_sotrudnik
                            , @param_id_sotrudnik
                            , @param_rpl_id_server)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@param_id_protocol", p.Id);
                    addCommand.Parameters.AddWithValue("@param_rpl_id_server", String.IsNullOrEmpty(sotrudnik.RplIdServer) ? "-1" : sotrudnik.RplIdServer);
                    addCommand.Parameters.AddWithValue("@param_id_rk_prtcl_sotrudnik", (String.IsNullOrEmpty(sotrudnik.Id) ? Convert.DBNull : sotrudnik.Id));
                    addCommand.Parameters.AddWithValue("@param_id_sotrudnik", (String.IsNullOrEmpty(sotrudnik.IdAuthor) ? Convert.DBNull : sotrudnik.IdAuthor));

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся сохранить RK_PRTCL_SOTRUDNIK с ID_PROTOCOL = " + p.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в RK_PRTCL_SOTRUDNIK с PROTOCOL_ID = " + p.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в RK_PRTCL_SOTRUDNIK с PROTOCOL_ID = " + p.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                //res
                CloudProtocolRes res = p.Res;

                if (res != null)
                {
                    addCommand.CommandText = 
                        @"insert into RK_PRTCL_RES 
                             (ID_PROTOCOL, RPL_ID_SERVER
                            , ID_RK_PRTCL_RES, CLIENT_NAME
                            , CLIENT_ADDRESS, RES_FACTORY_ID
                            , RES_LOCATION, RES_name
                            , RES_TYPE, RES_VID_ETS
                            , RES_RADIOCLASS, RES_FREQ_RESOLUTION_NUM
                            , RES_FREQ_RESOLUTION_DATE_START, RES_FREQ_RESOLUTION_DATE_END
                            , RES_CERTIFICATE_NUM, RES_CERTIFICATE_DATE_START
                            , RES_CERTIFICATE_DATE_END, RES_GEO_LAT
                            , RES_GEO_LONG, RES_ANTENNAHEIGHT
                            , RES_POWER, GEO_LAT_DELTA
                            , GEO_LAT_PERMIT, GEO_LONG_DELTA
                            , GEO_LONG_PERMIT, ANTHEIGHT_DELTA
                            , ANTHEIGHT_PERMIT, NAME_TYPE_RADIOBUTTON
                            , MANUAL_BAND_DELTA, MANUAL_BAND_DELTATYPE
                            , MANUAL_FREQ_DELTA, MANUAL_FREQ_DELTATYPE
                            , RPL_ID_SERVER) 
                          VALUES 
                             (@PARAM_ID_PROTOCOL, @PARAM_RPL_ID_SERVER
                            , @PARAM_ID_RK_PRTCL_RES, @PARAM_CLIENT_NAME
                            , @PARAM_CLIENT_ADDRESS, @PARAM_RES_FACTORY_ID
                            , @PARAM_RES_LOCATION, @PARAM_RES_name
                            , @PARAM_RES_TYPE, @PARAM_RES_VID_ETS
                            , @PARAM_RES_RADIOCLASS, @PARAM_RES_FREQ_RESOLUTION_NUM
                            , @PARAM_RES_FREQ_RESOLUTION_DATE_START, @PARAM_RES_FREQ_RESOLUTION_DATE_END
                            , @PARAM_RES_CERTIFICATE_NUM, @PARAM_RES_CERTIFICATE_DATE_START
                            , @PARAM_RES_CERTIFICATE_DATE_END, @PARAM_RES_GEO_LAT
                            , @PARAM_RES_GEO_LONG, @PARAM_RES_ANTENNAHEIGHT
                            , @PARAM_RES_POWER, @PARAM_GEO_LAT_DELTA
                            , @PARAM_GEO_LAT_PERMIT, @PARAM_GEO_LONG_DELTA
                            , @PARAM_GEO_LONG_PERMIT, @PARAM_ANTHEIGHT_DELTA
                            , @PARAM_ANTHEIGHT_PERMIT, @PARAM_NAME_TYPE_RADIOBUTTON
                            , @PARAM_MANUAL_BAND_DELTA, @PARAM_MANUAL_BAND_DELTATYPE
                            , @PARAM_MANUAL_FREQ_DELTA, @PARAM_MANUAL_FREQ_DELTATYPE
                            , @PARAM_RPL_ID_SERVER)";

                    addCommand.Parameters.Clear();
                    addCommand.Parameters.AddWithValue("@PARAM_ID_PROTOCOL", p.Id);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", -1);
                    addCommand.Parameters.AddWithValue("@PARAM_ID_RK_PRTCL_RES", (String.IsNullOrEmpty(res.Id) ? Convert.DBNull : res.Id));
                    addCommand.Parameters.AddWithValue("@PARAM_CLIENT_NAME", (String.IsNullOrEmpty(res.ClientName) ? Convert.DBNull : res.ClientName));
                    addCommand.Parameters.AddWithValue("@PARAM_CLIENT_ADDRESS", (String.IsNullOrEmpty(res.ClientAddress) ? Convert.DBNull : res.ClientAddress));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_FACTORY_ID", (String.IsNullOrEmpty(res.FactoryId) ? Convert.DBNull : res.FactoryId));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_LOCATION", (String.IsNullOrEmpty(res.Location) ? Convert.DBNull : res.Location));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_name", (String.IsNullOrEmpty(res.Name) ? Convert.DBNull : res.Name));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_TYPE", (String.IsNullOrEmpty(res.ResType) ? Convert.DBNull : res.ResType));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_VID_ETS", (String.IsNullOrEmpty(res.EtsVid) ? Convert.DBNull : res.EtsVid));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_RADIOCLASS", (String.IsNullOrEmpty(res.RadioClass) ? Convert.DBNull : res.RadioClass));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_FREQ_RESOLUTION_NUM", (String.IsNullOrEmpty(res.FreqResolutionNumber) ? Convert.DBNull : res.FreqResolutionNumber));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_FREQ_RESOLUTION_DATE_START", res.FreqResolutionStartDate);
                    addCommand.Parameters.AddWithValue("@PARAM_RES_FREQ_RESOLUTION_DATE_END", res.FreqResolutionEndDate);
                    addCommand.Parameters.AddWithValue("@PARAM_RES_CERTIFICATE_NUM", (String.IsNullOrEmpty(res.CertificateNumber) ? Convert.DBNull : res.CertificateNumber));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_CERTIFICATE_DATE_START", res.CertificateStartDate);
                    addCommand.Parameters.AddWithValue("@PARAM_RES_CERTIFICATE_DATE_END", res.CertificateEndDate);
                    addCommand.Parameters.AddWithValue("@PARAM_RES_GEO_LAT", (String.IsNullOrEmpty(res.GeoLat) ? Convert.DBNull : res.GeoLat));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_GEO_LONG", (String.IsNullOrEmpty(res.GeoLong) ? Convert.DBNull : res.GeoLong));
                    addCommand.Parameters.AddWithValue("@PARAM_RES_ANTENNAHEIGHT", res.AntennaHeight);
                    addCommand.Parameters.AddWithValue("@PARAM_RES_POWER", res.Power);
                    addCommand.Parameters.AddWithValue("@PARAM_GEO_LAT_DELTA", (String.IsNullOrEmpty(res.GeoLatDelta) ? Convert.DBNull : res.GeoLatDelta));
                    addCommand.Parameters.AddWithValue("@PARAM_GEO_LAT_PERMIT", (String.IsNullOrEmpty(res.GeoLatPermit) ? Convert.DBNull : res.GeoLatPermit));
                    addCommand.Parameters.AddWithValue("@PARAM_GEO_LONG_DELTA", (String.IsNullOrEmpty(res.GeoLongDelta) ? Convert.DBNull : res.GeoLongDelta));
                    addCommand.Parameters.AddWithValue("@PARAM_GEO_LONG_PERMIT", (String.IsNullOrEmpty(res.GeoLongPermit) ? Convert.DBNull : res.GeoLongPermit));
                    addCommand.Parameters.AddWithValue("@PARAM_ANTHEIGHT_DELTA", res.AntennaHeightDelta);
                    addCommand.Parameters.AddWithValue("@PARAM_ANTHEIGHT_PERMIT", res.AntennaHeightPermit);
                    addCommand.Parameters.AddWithValue("@PARAM_NAME_TYPE_RADIOBUTTON", res.VisualTypeSwitch);
                    addCommand.Parameters.AddWithValue("@PARAM_MANUAL_BAND_DELTA", res.ManualBandDelta);
                    addCommand.Parameters.AddWithValue("@PARAM_MANUAL_BAND_DELTATYPE", res.ManualBandDeltaType);
                    addCommand.Parameters.AddWithValue("@PARAM_MANUAL_FREQ_DELTA", res.ManualFreqDelta);
                    addCommand.Parameters.AddWithValue("@PARAM_MANUAL_FREQ_DELTATYPE", res.ManualFreqDeltaType);
                    addCommand.Parameters.AddWithValue("@PARAM_RPL_ID_SERVER", String.IsNullOrEmpty(res.RplIdServer) ? "-1" : res.RplIdServer);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся сохранить RK_PRTCL_RES с ID_PROTOCOL = " + p.Id);
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (SqlParameter param in addCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommand.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при вставке записи в RK_PRTCL_RES с PROTOCOL_ID = " + p.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при вставке записи в RK_PRTCL_RES с PROTOCOL_ID = " + p.Id);
                        //Log.Write(e.Message);

                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                //"select PR.ID_RK_PRTCL_RES, PR.CLIENT_NAME, PR.CLIENT_ADDRESS, PR.RES_FACTORY_ID, PR.RES_LOCATION, PR.RES_name, PR.RES_TYPE, " +
                //                        "PR.RES_VID_ETS, PR.RES_RADIOCLASS, PR.RES_FREQ_RESOLUTION_NUM, PR.RES_FREQ_RESOLUTION_DATE_START, PR.RES_FREQ_RESOLUTION_DATE_END," +
                //                        "PR.RES_CERTIFICATE_NUM, PR.RES_CERTIFICATE_DATE_START, PR.RES_CERTIFICATE_DATE_END, PR.RES_GEO_LAT, PR.RES_GEO_LONG, PR.RES_ANTENNAHEIGHT," +
                //                        "PR.RES_POWER, PR.GEO_LAT_DELTA, PR.GEO_LAT_PERMIT, PR.GEO_LONG_DELTA, pr.GEO_LONG_PERMIT, PR.ANTHEIGHT_DELTA, PR.ANTHEIGHT_PERMIT," +
                //                        "PR.NAME_TYPE_RADIOBUTTON, PR.MANUAL_BAND_DELTA, PR.MANUAL_BAND_DELTATYPE, PR.MANUAL_FREQ_DELTA, PR.MANUAL_FREQ_DELTATYPE " +
                //                        " from RK_PRTCL_RES PR " +
                //                        " where PR.ID_PROTOCOL = " + entityId;

                conn.Close();
            }
            return 0;
        }

        public static int ReplicateMkrk(MkrkEntity m, DateTime maxDate)
        {
            string sqlStringProject = "";
            string sqlStringObject = "";
            string sqlStringData = "";

            if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
            {
                if (EntityFound("MKRK", m.Id, maxDate))
                {
                    //UPDATE
                    sqlStringObject = 
                        @"update MON_OBJECTS 
                          set 
                              id_mon_objects = @param_id_mon_objects
                            , name = @param_name
                            , obj_id = @param_obj_id
                            , last_time = @param_last_time
                            , last_lon = @param_last_lon
                            , last_lat = @param_last_lat
                            , last_speed = @param_speed
                            , last_add_info = @param_add_info
                            , proj_id = @param_proj_id
                            , phone = @param_phone
                            , id_mon_projects = @param_id_mon_projects
                            , id_rko = @param_id_rko
                            , rpl_id_server = @param_rpl_id_server 
                          where id_mon_objects = " + m.Id;
                }
                else
                {
                    //INSERT
                    sqlStringObject = 
                        @"insert into MON_OBJECTS 
                             (id_mon_objects
                            , name
                            , obj_id
                            , last_time
                            , last_lon
                            , last_lat
                            , last_speed
                            , last_add_info
                            , proj_id
                            , phone
                            , id_mon_projects
                            , id_rko
                            , rpl_id_server)
                          values 
                             (@param_id_mon_objects
                            , @param_name
                            , @param_obj_id
                            , @param_last_time
                            , @param_last_lon
                            , @param_last_lat
                            , @param_last_speed
                            , @param_last_add_info
                            , @param_proj_id
                            , @param_phone
                            , @param_id_mon_projects
                            , @param_id_rko, @param_rpl_id_server)";
                }

                FbConnection fbconn = new FbConnection(_ClientSettings.ClientConnectionString);
                fbconn.Open();

                // if (!RecordFound("MON_PROJECTS", "ID_MON_PROJECTS", m.IdProject, settings))
                {
                    //insert into projects
                    sqlStringProject = 
                        @"update or insert into mon_projects 
                             (id_mon_projects
                            , proj_id
                            , name
                            , PASSWORD
                            , id_region
                            , rpl_id_server
                            , last_time) 
                          values 
                             (@param_id_mon_projects
                            , @param_proj_id
                            , @param_name
                            , @param_PASSWORD 
                            , @param_id_region
                            , @param_rpl_id_server
                            , @param_last_time) 
                          matching (id_mon_projects)";// + m.IdProject;

                    FbTransaction fbtranProj = fbconn.BeginTransaction();
                    FbCommand addProjCommand = new FbCommand(sqlStringProject, fbconn);
                    addProjCommand.Connection = fbconn;
                    addProjCommand.Transaction = fbtranProj;
                    addProjCommand.Parameters.AddWithValue("@param_id_mon_projects", String.IsNullOrEmpty(m.IdProject) ? Convert.DBNull : m.IdProject);
                    addProjCommand.Parameters.AddWithValue("@param_proj_id", String.IsNullOrEmpty(m.ProjId) ? Convert.DBNull : m.ProjId);
                    addProjCommand.Parameters.AddWithValue("@param_name", String.IsNullOrEmpty(m.ProjName) ? Convert.DBNull : m.ProjName);
                    addProjCommand.Parameters.AddWithValue("@param_PASSWORD", String.IsNullOrEmpty(m.Password) ? Convert.DBNull : m.Password);
                    addProjCommand.Parameters.AddWithValue("@param_id_region", String.IsNullOrEmpty(m.IdRegion) ? Convert.DBNull : m.IdRegion);
                    addProjCommand.Parameters.AddWithValue("@param_rpl_id_server", String.IsNullOrEmpty(m.RplIdServer) ?  "-1" : m.RplIdServer);//String.IsNullOrEmpty(m.IdProject) ? Convert.DBNull : m.IdProject);
                    addProjCommand.Parameters.AddWithValue("@param_last_time", m.DtModify);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся вставить MON_PROJECTS");
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (FbParameter param in addProjCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addProjCommand.ExecuteNonQuery();
                        fbtranProj.Commit();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при сохранении строки в MON_PROJECTS с ID_MON_PROJECT = " + m.IdProject);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при сохранении строки в MON_PROJECTS с ID_MON_PROJECT = " + m.IdProject);
                        //Log.Write(e.Message);

                        foreach (FbParameter param in addProjCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                    
                    addProjCommand.Dispose();
                }

                //выполнить sqlStringObject
                FbTransaction fbtran = fbconn.BeginTransaction();
                FbCommand addCommand = new FbCommand(sqlStringObject, fbconn);
                addCommand.Connection = fbconn;
                addCommand.Transaction = fbtran;
                addCommand.Parameters.AddWithValue("@param_id_mon_objects", m.Id);
                addCommand.Parameters.AddWithValue("@param_name", String.IsNullOrEmpty(m.ObjName) ? Convert.DBNull : m.ObjName); 
                addCommand.Parameters.AddWithValue("@param_obj_id", String.IsNullOrEmpty(m.ObjId) ? Convert.DBNull : m.ObjId);
                addCommand.Parameters.AddWithValue("@param_last_time", m.DtModify);
                addCommand.Parameters.AddWithValue("@param_last_lon", String.IsNullOrEmpty(m.Lon) ? Convert.DBNull : m.Lon.Replace(",","."));
                addCommand.Parameters.AddWithValue("@param_last_lat", String.IsNullOrEmpty(m.Lat) ? Convert.DBNull : m.Lat.Replace(",", "."));
                addCommand.Parameters.AddWithValue("@param_last_speed", String.IsNullOrEmpty(m.Speed) ? Convert.DBNull : m.Speed.Replace(",", "."));
                addCommand.Parameters.AddWithValue("@param_last_add_info", String.IsNullOrEmpty(m.AddInfo) ? Convert.DBNull : m.AddInfo);
                addCommand.Parameters.AddWithValue("@param_proj_id", String.IsNullOrEmpty(m.ProjId) ? Convert.DBNull :m.ProjId);
                addCommand.Parameters.AddWithValue("@param_phone", String.IsNullOrEmpty(m.Phone) ? Convert.DBNull : m.Phone);
                addCommand.Parameters.AddWithValue("@param_id_rko",String.IsNullOrEmpty(m.IdRko) ? Convert.DBNull : m.IdRko);
                addCommand.Parameters.AddWithValue("@param_id_mon_projects", String.IsNullOrEmpty(m.IdProject) ? Convert.DBNull : m.IdProject);                    
                addCommand.Parameters.AddWithValue("@param_rpl_id_server", String.IsNullOrEmpty(m.RplIdServer) ? "-1" : m.RplIdServer);

                try
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Пытаемся вставить MON_OBJECTS");
                    }

                    if (_ClientSettings.Verbose)
                    {
                        foreach (FbParameter param in addCommand.Parameters)
                        {
                            logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    addCommand.ExecuteNonQuery();
                    fbtran.Commit();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при сохранении строки в MON_OBJECTS с ID_MON_OBJECT = " + m.Id);
                    logger.Error(e.Message);
                    //Log.Write("Произошла ошибка при сохранении строки в MON_OBJECTS с ID_MON_OBJECT = " + m.Id);
                    //Log.Write(e.Message);

                    foreach (FbParameter param in addCommand.Parameters)
                    {
                        logger.Error(param.ParameterName + " = " + param.Value.ToString());
                    }
                }

                //вставить в  MON_DATA те, что пришли в m
                sqlStringData =
                    @"insert into MON_DATA 
                         (obj_id
                        , proj_id
                        , mon_time
                        , lon
                        , lat
                        , speed
                        , add_info
                        , id_mon_objects
                        , rpl_id_server) 
                      values 
                         (@param_obj_id
                        , @param_proj_id
                        , @param_mon_time
                        , @param_lon
                        , @param_lat
                        , @param_speed
                        , @param_add_info
                        , @param_id_mon_objects
                        , @param_rpl_id_server)";

                FbCommand addCommandData = new FbCommand(sqlStringData, fbconn);
                FbTransaction fbtranData = fbconn.BeginTransaction();
                
                addCommandData.Transaction = fbtranData;

                foreach (MkrkData d in m.Data)
                {
                    addCommandData.Parameters.Clear();
                    addCommandData.Parameters.AddWithValue("param_obj_id", String.IsNullOrEmpty(m.ObjId) ? Convert.DBNull : m.ObjId);
                    addCommandData.Parameters.AddWithValue("param_proj_id", String.IsNullOrEmpty(m.ProjId) ? Convert.DBNull : m.ProjId);
                    addCommandData.Parameters.AddWithValue("param_mon_time", d.MonTime);
                    addCommandData.Parameters.AddWithValue("param_lon", String.IsNullOrEmpty(d.Lon) ? Convert.DBNull : d.Lon.Replace(",", "."));
                    addCommandData.Parameters.AddWithValue("param_lat", String.IsNullOrEmpty(d.Lat) ? Convert.DBNull : d.Lat.Replace(",", "."));
                    addCommandData.Parameters.AddWithValue("param_speed", String.IsNullOrEmpty(d.Speed) ? Convert.DBNull : d.Speed.Replace(",", "."));
                    addCommandData.Parameters.AddWithValue("param_add_info", String.IsNullOrEmpty(d.AddInfo) ? Convert.DBNull : d.AddInfo);
                    addCommandData.Parameters.AddWithValue("param_id_mon_objects", String.IsNullOrEmpty(m.Id) ? Convert.DBNull : m.Id);
                    addCommandData.Parameters.AddWithValue("param_rpl_id_server", String.IsNullOrEmpty(m.RplIdServer) ?  "-1" : m.RplIdServer);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся вставить MON_DATA");
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (FbParameter param in addCommandData.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommandData.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при сохранении строки в MON_DATA с ID_MON_OBJECT = " + m.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при сохранении строки в MON_DATA с ID_MON_OBJECT = " + m.Id);
                        //Log.Write(e.Message);
                        foreach (FbParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    try
                    {
                        fbtranData.Commit();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при коммите транзакции при сохранении строки в MON_DATA с ID_MON_OBJECT = " + m.Id);
                        logger.Error(e.Message);
                        fbtranData.Rollback();
                    }
                }

                addCommandData.Dispose();
                fbconn.Close();
            }

            if (_ClientSettings.ClientProvider.ToUpper() == "MSSQL")
            {
                if (EntityFound("MKRK", m.Id, maxDate))
                {
                    //UPDATE
                    sqlStringObject = 
                        @"update MON_OBJECTS 
                          set
                              id_mon_objects = @param_id_mon_objects
                            , name = @param_name
                            , obj_id = @param_obj_id
                            , last_time = @param_last_time
                            , last_lon = @param_last_lon
                            , last_lat = @param_last_lat
                            , last_speed = @param_speed
                            , last_add_info = @param_add_info
                            , proj_id = @param_proj_id
                            , phone = @param_phone
                            , id_mon_projects = @param_id_mon_projects
                            , id_rko = @param_id_rko
                            , rpl_id_server = @param_rpl_id_server 
                          where id_mon_objects = " + m.Id;
                }
                else
                {
                    //INSERT
                    sqlStringObject = 
                        @"insert into MON_OBJECTS 
                             (id_mon_objects
                            , name
                            , obj_id
                            , last_time
                            , last_lon
                            , last_lat
                            , last_speed
                            , last_add_info
                            , proj_id
                            , phone
                            , id_mon_projects
                            , id_rko
                            , rpl_id_server)
                          values 
                             (@param_id_mon_objects
                            , @param_name
                            , @param_obj_id
                            , @param_last_time
                            , @param_last_lon
                            , @param_last_lat
                            , @param_last_speed
                            , @param_last_add_info
                            , @param_proj_id
                            , @param_phone
                            , @param_id_mon_projects
                            , @param_id_rko
                            , @param_rpl_id_server)";
                }

                if (!RecordFound("MON_PROJECTS", "ID_MON_PROJECTS", m.IdProject))
                {
                    //insert into projects
                    sqlStringProject = 
                        @"insert into MON_PROJECTS 
                             (id_mon_projects
                            , proj_id
                            , name
                            , PASSWORD
                            , id_region
                            , rpl_id_server
                            , last_time) 
                          values 
                             (@param_id_mon_projects
                            , @param_proj_id
                            , @param_name
                            , @param_PASSWORD 
                            , @param_id_region
                            , @param_rpl_id_server
                            , @param_last_time)";
                }
                else
                {
                    //update projects
                    sqlStringProject = 
                        @"update MON_PROJECTS 
                          set 
                              proj_id = @param_proj_id
                            , name = @param_name
                            , PASSWORD = @param_PASSWORD
                            , id_region = @param_id_region
                            , rpl_id_server = @param_rpl_id_server
                            , last_time =  @param_last_time
                          where id_mon_projects = @param_id_mon_projects";
                }

                //выполнить sqlStringProject
                SqlConnection conn = new SqlConnection(_ClientSettings.ClientConnectionString);
                SqlCommand addCommand = new SqlCommand(sqlStringProject, conn);
                addCommand.Parameters.Clear();
                addCommand.Parameters.AddWithValue("@param_id_mon_projects", m.IdProject);
                addCommand.Parameters.AddWithValue("@param_proj_id", String.IsNullOrEmpty(m.ProjId) ? Convert.DBNull : m.ProjId);
                addCommand.Parameters.AddWithValue("@param_name", String.IsNullOrEmpty(m.ProjName) ? Convert.DBNull : m.ProjName);
                addCommand.Parameters.AddWithValue("@param_PASSWORD", String.IsNullOrEmpty(m.Password) ? Convert.DBNull : m.Password);
                addCommand.Parameters.AddWithValue("@param_id_region", String.IsNullOrEmpty(m.IdRegion) ? Convert.DBNull : m.IdRegion);
                addCommand.Parameters.AddWithValue("@param_rpl_id_server",  String.IsNullOrEmpty(m.RplIdServer) ?  "-1" : m.RplIdServer);
                addCommand.Parameters.AddWithValue("@param_last_time", m.DtModify);                
                conn.Open();

                try
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Пытаемся вставить проект в MON_PROJECTS с ID =" + m.IdProject);
                    }

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при вставке проекта в MON_PROJECTS с ID =" + m.IdProject);
                    logger.Error(e.Message);
                    //Log.Write("Произошла ошибка при вставке проекта в MON_PROJECTS с ID =" + m.IdProject);
                    //Log.Write(e.Message);

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                //выполнить sqlStringObject
                addCommand.CommandText = sqlStringObject;
                addCommand.Parameters.Clear();
                //@param_id_mon_objects, @param_name, @param_obj_id, @param_last_time, @param_last_lon, @param_last_lat, @param_last_speed, @param_last_add_info," +
                                      //" @param_proj_id, @param_phone, @param_id_mon_projects," +
                                      //"@param_id_rko, @param_rpl_id_server

                addCommand.Parameters.AddWithValue("param_id_mon_objects", m.Id);
                addCommand.Parameters.AddWithValue("param_name", String.IsNullOrEmpty(m.ObjName) ? Convert.DBNull : m.ObjName);
                addCommand.Parameters.AddWithValue("param_obj_id", String.IsNullOrEmpty(m.ObjId) ? Convert.DBNull : m.ObjId);
                addCommand.Parameters.AddWithValue("param_last_time", m.DtModify);
                addCommand.Parameters.AddWithValue("param_last_lon", String.IsNullOrEmpty(m.Lon) ? Convert.DBNull : m.Lon.Replace(",","."));
                addCommand.Parameters.AddWithValue("param_last_lat", String.IsNullOrEmpty(m.Lat) ? Convert.DBNull : m.Lat.Replace(",","."));
                addCommand.Parameters.AddWithValue("param_last_speed", String.IsNullOrEmpty(m.Speed) ? Convert.DBNull : m.Speed.Replace(",","."));
                addCommand.Parameters.AddWithValue("param_last_add_info", String.IsNullOrEmpty(m.AddInfo) ? Convert.DBNull : m.AddInfo);
                addCommand.Parameters.AddWithValue("param_proj_id", String.IsNullOrEmpty(m.ProjId) ? Convert.DBNull : m.ProjId);
                addCommand.Parameters.AddWithValue("param_phone", String.IsNullOrEmpty(m.Phone) ? Convert.DBNull : m.Phone);
                addCommand.Parameters.AddWithValue("param_id_mon_projects", String.IsNullOrEmpty(m.IdProject) ? Convert.DBNull : m.IdProject);
                addCommand.Parameters.AddWithValue("param_id_rko", String.IsNullOrEmpty(m.IdRko) ? Convert.DBNull : m.IdRko);
                addCommand.Parameters.AddWithValue("param_rpl_id_server", String.IsNullOrEmpty(m.RplIdServer) ?  "-1" : m.RplIdServer);

                try
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Пытаемся вставить объект в MON_OBJECTS с ID = " + m.Id);
                    }

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    addCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при вставке объекта МКРК в MON_OBJECTS с ID =" + m.Id);
                    logger.Error(e.Message);
                    //Log.Write("Произошла ошибка при вставке объекта МКРК в MON_OBJECTS с ID =" + m.Id);
                    //Log.Write(e.Message);

                    if (_ClientSettings.Verbose)
                    {
                        foreach (SqlParameter param in addCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }
                
                conn.Close();
            }
            
            return 0;
        }

        public static int ReplicateMkrkFromServerMkrk(MkrkEntity m, DateTime maxDate)
        {
            string sqlStringProject = "";
            string sqlStringObject = "";
            string sqlStringData = "";

            if (_ClientSettings.ClientProvider.ToUpper() == "FIREBIRD")
            {
                sqlStringObject = 
                    @"update or insert into MON_OBJECTS 
                         (name
                        , obj_id
                        , last_time
                        , last_lon
                        , last_lat
                        , last_speed
                        , last_add_info
                        , proj_id
                        , phone
                        , id_mon_projects
                        , rpl_id_server)
                      values 
                         (@param_name
                        , @param_obj_id
                        , @param_last_time
                        , @param_last_lon
                        , @param_last_lat
                        , @param_last_speed
                        , @param_last_add_info
                        , @param_proj_id
                        , @param_phone
                        , @param_id_mon_projects
                        , @param_rpl_id_server) 
                      matching (obj_id, proj_id) ";

                sqlStringProject = 
                    @"update or insert into mon_projects 
                         (proj_id
                        , name
                        , PASSWORD
                        , id_region
                        , rpl_id_server
                        , last_time) 
                      values 
                         (@param_proj_id
                        , @param_name
                        , @param_PASSWORD 
                        , @param_id_region
                        , @param_rpl_id_server
                        , @param_last_time) 
                      matching (proj_id)"; // + m.IdProject;

                    //INSERT
                    //sqlStringObject = "insert into MON_OBJECTS " +
                    //                  "  ( id_mon_objects, name, obj_id, last_time, last_lon, last_lat, last_speed, last_add_info, proj_id, phone, id_mon_projects," +
                    //                  "id_rko, rpl_id_server )" +
                    //                  " values ( @param_id_mon_objects, @param_name, @param_obj_id, @param_last_time, @param_last_lon, @param_last_lat, @param_last_speed, @param_last_add_info," +
                    //                  " @param_proj_id, @param_phone, @param_id_mon_projects," +
                    //                  "@param_id_rko, @param_rpl_id_server) ";

                FbConnection fbconn = new FbConnection(_ClientSettings.ClientConnectionString);
                fbconn.Open();
                {
                    //insert into projects
                    FbTransaction fbtranProj = fbconn.BeginTransaction();
                    FbCommand addProjCommand = new FbCommand(sqlStringProject, fbconn);
                    addProjCommand.Connection = fbconn;
                    addProjCommand.Transaction = fbtranProj;
                    //addProjCommand.Parameters.AddWithValue("@param_id_mon_projects", Convert.DBNull);
                    addProjCommand.Parameters.AddWithValue("@param_proj_id", String.IsNullOrEmpty(m.ProjId) ? Convert.DBNull : m.ProjId);
                    addProjCommand.Parameters.AddWithValue("@param_name", String.IsNullOrEmpty(m.ProjName) ? Convert.DBNull : m.ProjName);
                    addProjCommand.Parameters.AddWithValue("@param_PASSWORD", String.IsNullOrEmpty(m.Password) ? Convert.DBNull : m.Password);
                    addProjCommand.Parameters.AddWithValue("@param_id_region", String.IsNullOrEmpty(m.IdRegion) ? Convert.DBNull : m.IdRegion);
                    addProjCommand.Parameters.AddWithValue("@param_rpl_id_server",  String.IsNullOrEmpty(m.RplIdServer) ?  "-1" : m.RplIdServer);//String.IsNullOrEmpty(m.IdProject) ? Convert.DBNull : m.IdProject);
                    addProjCommand.Parameters.AddWithValue("@param_last_time", m.DtModify);

                    try
                    {
                        if (_ClientSettings.Verbose) logger.Trace("Пытаемся вставить MON_PROJECTS");

                        if (_ClientSettings.Verbose)
                            foreach (FbParameter param in addProjCommand.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        addProjCommand.ExecuteNonQuery();
                        fbtranProj.Commit();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при сохранении строки в MON_PROJECTS с ID_MON_PROJECT = " + m.IdProject);
                        logger.Error(e.Message);

                        foreach (FbParameter param in addProjCommand.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    addProjCommand.Dispose();
                }

                //выполнить sqlStringObject
                FbTransaction fbtran = fbconn.BeginTransaction();
                FbCommand addCommand = new FbCommand(sqlStringObject, fbconn);
                addCommand.Connection = fbconn;
                addCommand.Transaction = fbtran;
                //addCommand.Parameters.AddWithValue("@param_id_mon_objects", Convert.DBNull);
                addCommand.Parameters.AddWithValue("@param_name", String.IsNullOrEmpty(m.ObjName) ? Convert.DBNull : m.ObjName);
                addCommand.Parameters.AddWithValue("@param_obj_id", String.IsNullOrEmpty(m.ObjId) ? Convert.DBNull : m.ObjId);
                addCommand.Parameters.AddWithValue("@param_last_time", m.DtModify);
                addCommand.Parameters.AddWithValue("@param_last_lon", String.IsNullOrEmpty(m.Lon) ? Convert.DBNull : m.Lon.Replace(",", "."));
                addCommand.Parameters.AddWithValue("@param_last_lat", String.IsNullOrEmpty(m.Lat) ? Convert.DBNull : m.Lat.Replace(",", "."));
                addCommand.Parameters.AddWithValue("@param_last_speed", String.IsNullOrEmpty(m.Speed) ? Convert.DBNull : m.Speed.Replace(",", "."));
                addCommand.Parameters.AddWithValue("@param_last_add_info", String.IsNullOrEmpty(m.AddInfo) ? Convert.DBNull : m.AddInfo);
                addCommand.Parameters.AddWithValue("@param_proj_id", String.IsNullOrEmpty(m.ProjId) ? Convert.DBNull : m.ProjId);
                addCommand.Parameters.AddWithValue("@param_phone", String.IsNullOrEmpty(m.Phone) ? Convert.DBNull : m.Phone);
                addCommand.Parameters.AddWithValue("@param_id_rko", String.IsNullOrEmpty(m.IdRko) ? Convert.DBNull : m.IdRko);
                addCommand.Parameters.AddWithValue("@param_id_mon_projects", String.IsNullOrEmpty(m.IdProject) ? Convert.DBNull : m.IdProject);
                addCommand.Parameters.AddWithValue("@param_rpl_id_server", String.IsNullOrEmpty(m.RplIdServer) ? "-1" : m.RplIdServer);

                try
                {
                    if (_ClientSettings.Verbose)
                    {
                        logger.Trace("Пытаемся вставить MON_OBJECTS");
                    }

                    if (_ClientSettings.Verbose)
                    {
                        foreach (FbParameter param in addCommand.Parameters)
                        {
                            logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }

                    addCommand.ExecuteNonQuery();
                    fbtran.Commit();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при сохранении строки в MON_OBJECTS с ID_MON_OBJECT = " + m.Id);
                    logger.Error(e.Message);
                    //Log.Write("Произошла ошибка при сохранении строки в MON_OBJECTS с ID_MON_OBJECT = " + m.Id);
                    //Log.Write(e.Message);

                    foreach (FbParameter param in addCommand.Parameters)
                    {
                        logger.Error(param.ParameterName + " = " + param.Value.ToString());
                    }
                }

                addCommand.Dispose();
                //вставить в  MON_DATA те, что пришли в m
                sqlStringData =
                    @"insert into MON_DATA 
                         (obj_id, proj_id
                        , mon_time
                        , lon
                        , lat
                        , speed
                        , add_info
                        , id_mon_objects
                        , rpl_id_server) 
                      values 
                         (@param_obj_id
                        , @param_proj_id
                        , @param_mon_time
                        , @param_lon
                        , @param_lat
                        , @param_speed
                        , @param_add_info
                        , @param_id_mon_objects
                        , @param_rpl_id_server)";

                FbTransaction fbtranData = fbconn.BeginTransaction();
                FbCommand addCommandData = new FbCommand(sqlStringData, fbconn);
                addCommandData.CommandText = sqlStringData;
                addCommandData.Transaction = fbtranData;

                foreach (MkrkData d in m.Data)
                {
                    addCommandData.Parameters.Clear();
                    addCommandData.Parameters.AddWithValue("param_obj_id", String.IsNullOrEmpty(m.ObjId) ? Convert.DBNull : m.ObjId);
                    addCommandData.Parameters.AddWithValue("param_proj_id", String.IsNullOrEmpty(m.ProjId) ? Convert.DBNull : m.ProjId);
                    addCommandData.Parameters.AddWithValue("param_mon_time", d.MonTime);
                    addCommandData.Parameters.AddWithValue("param_lon", String.IsNullOrEmpty(d.Lon) ? Convert.DBNull : d.Lon.Replace(",", "."));
                    addCommandData.Parameters.AddWithValue("param_lat", String.IsNullOrEmpty(d.Lat) ? Convert.DBNull : d.Lat.Replace(",", "."));
                    addCommandData.Parameters.AddWithValue("param_speed", String.IsNullOrEmpty(d.Speed) ? Convert.DBNull : d.Speed.Replace(",", "."));
                    addCommandData.Parameters.AddWithValue("param_add_info", String.IsNullOrEmpty(d.AddInfo) ? Convert.DBNull : d.AddInfo);
                    addCommandData.Parameters.AddWithValue("param_id_mon_objects", String.IsNullOrEmpty(m.Id) ? Convert.DBNull : m.Id);
                    addCommandData.Parameters.AddWithValue("param_rpl_id_server", String.IsNullOrEmpty(m.RplIdServer) ? "-1" : m.RplIdServer);

                    try
                    {
                        if (_ClientSettings.Verbose)
                        {
                            logger.Trace("Пытаемся вставить MON_DATA");
                        }

                        if (_ClientSettings.Verbose)
                        {
                            foreach (FbParameter param in addCommandData.Parameters)
                            {
                                logger.Trace(param.ParameterName + " = " + param.Value.ToString());
                            }
                        }

                        addCommandData.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        logger.Error("Произошла ошибка при сохранении строки в MON_DATA с ID_MON_OBJECT = " + m.Id);
                        logger.Error(e.Message);
                        //Log.Write("Произошла ошибка при сохранении строки в MON_DATA с ID_MON_OBJECT = " + m.Id);
                        //Log.Write(e.Message);

                        foreach (FbParameter param in addCommandData.Parameters)
                        {
                            logger.Error(param.ParameterName + " = " + param.Value.ToString());
                        }
                    }
                }

                try
                {
                    fbtranData.Commit();
                }
                catch (Exception e)
                {
                    logger.Error("Произошла ошибка при коммите транзакции при сохранении строки в MON_DATA с ID_MON_OBJECT = " + m.Id);
                    logger.Error(e.Message);
                    fbtranData.Rollback();
                }

                fbconn.Close();
            }

            return 0;
        }

        static void Main(string[] args)
        {
            //var settings = new ClientSettings();
            //settings.ClientConnectionString = "server=ZLOJ-PC\\SRVZERO;database=ASRK_RF_MSK;uid=sa;pwd=123";
            //settings.ClientProvider = "MsSql";
            //settings.ServicePort = 30001;
            //settings.ServiceUrl = "http://localhost";
            //settings.EntityCategoryList = new List<EntityCategory>();
            //settings.EntityCategoryList.Add(new EntityCategory(
            //    "PROTOCOLS",null));
            //settings.EntityCategoryList.Add(new EntityCategory(
            //    "ACTS", null));
            //using (Stream writer = new FileStream("client_settings.xml", FileMode.Create))
            //{
            //    XmlSerializer xmlSerializer = new XmlSerializer(typeof(ClientSettings));
            //    xmlSerializer.Serialize(writer, settings);
            //}
            Stream stream = new FileStream("client_settings.xml", FileMode.OpenOrCreate);
            XmlSerializer serializer = new XmlSerializer(typeof(ClientSettings));
            _ClientSettings = (ClientSettings)serializer.Deserialize(stream);

            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            binding.SendTimeout = new TimeSpan(1, 1, 1);
            binding.MaxReceivedMessageSize = 2147483647;

            var client = new ReplicationServiceReference.ReplicationServiceClient(binding,
                new EndpointAddress(string.Format("{0}:{1}", _ClientSettings.ServiceUrl, _ClientSettings.ServicePort.ToString())));

            //var logfile = new System.IO.StreamWriter("log0.txt");
            //смотрим параметр - насколько подробно логировать действия программы
            _ClientSettings.Verbose = false;

            foreach (var s in args)
            {
                if (s.ToLower() == "-v") _ClientSettings.Verbose = true;
            }

            foreach (var currentEntity in _ClientSettings.EntityCategoryList)
            {
                DateTime maxDate = currentEntity.MaxDate ?? GetMaxDate(currentEntity.Name, "");
                var entityList = client.GetEntityIdList(currentEntity.Name, maxDate);
                logger.Info("Получено " + entityList.Count() + " сущностей класса " + currentEntity.Name.ToUpper() + "  ");
                logger.Info("Получено " + entityList.Count() + " сущностей класса " + currentEntity.Name.ToUpper() + "  ");
                foreach (var currentEntityId in entityList)
                {
                    if (currentEntity.Name.ToUpper() == "PROTOCOLS")
                    {
                        ProtocolEntity protocolEntity = new ProtocolEntity();
                        bool requestSuccessfull = true;

                        try
                        {
                            protocolEntity = client.GetProtocolEntity(currentEntityId.ToString());
                        }
                        catch (Exception e)
                        {
                            requestSuccessfull = false;
                            logger.Error("Возникла ошибка получения данных от сервера для " + currentEntity.Name +
                                            " с ID = " + currentEntityId.ToString());                                  
                            //Log.Write("Возникла ошибка получения данных от сервера для " + currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }

                        if ((requestSuccessfull) && (! String.IsNullOrWhiteSpace(protocolEntity.Id.ToString())))
                        {
                            logger.Info("обрабатывается " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("обрабатывается " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                            ReplicateProtocol(protocolEntity, maxDate);
                        }
                        else
                        {
                            logger.Error("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Не получен ответ от сервера. Отмена вставки "+ currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }
                    }

                    if (currentEntity.Name.ToUpper() == "ACTS")
                    {
                        ActEntity actEntity = new ActEntity();
                        bool requestSuccessfull = true;

                        try
                        {
                            actEntity = client.GetActEntity(currentEntityId.ToString());
                        }
                        catch 
                        {
                            requestSuccessfull = false;
                            logger.Error
                                ("Возникла ошибка получения данных от сервера для " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Возникла ошибка получения данных от сервера для " + currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }

                        if ((requestSuccessfull) && (! String.IsNullOrWhiteSpace(actEntity.Id)))
                        {
                            logger.Info("обрабатывается " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("обрабатывается " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                            ReplicateAct(actEntity, maxDate);
                        }
                        else
                        {
                            logger.Error("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }
                    }

                    if (currentEntity.Name.ToUpper() == "RKO")
                    {
                        RkoEntity rkoEntity = new RkoEntity();
                        bool requestSuccessfull = true;

                        try
                        {
                            rkoEntity  = client.GetOldSchemaRkoEntity(currentEntityId.ToString());
                        }
                        catch
                        {
                            requestSuccessfull = false;
                            logger.Error("Возникла ошибка получения данных от сервера для " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Возникла ошибка получения данных от сервера для " + currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }

                        if ((requestSuccessfull) && (! String.IsNullOrWhiteSpace(rkoEntity.Id)))
                        {
                            logger.Info("обрабатывается " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("обрабатывается " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                            ReplicateRko(rkoEntity, maxDate);
                        }
                        else
                        {
                            logger.Error("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }
                    }

                    if (currentEntity.Name.ToUpper() == "EMPLOYEE")
                    {
                        EmployeeEntity employeeEntity = new EmployeeEntity();
                        bool requestSuccessfull = true;

                        try
                        {
                            employeeEntity = client.GetEmployeeEntity(currentEntityId.ToString());
                        }
                        catch
                        {
                            requestSuccessfull = false;
                            logger.Error("Возникла ошибка получения данных от сервера для " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Возникла ошибка получения данных от сервера для " + currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }

                        if ((requestSuccessfull) && (! String.IsNullOrWhiteSpace(employeeEntity.Id)))
                        {
                            logger.Info("обрабатывается " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("обрабатывается " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                            ReplicateEmployee(employeeEntity, maxDate);
                        }
                        else
                        {
                            logger.Error("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }
                    }

                    if (currentEntity.Name.ToUpper() == "PLANS")
                    {
                        PlanDetailEntity planDetailEntity = new PlanDetailEntity();
                        bool requestSuccessfull = true;

                        try
                        {
                            planDetailEntity = client.GetPlanDetailEntity(currentEntityId.ToString());
                        }
                        catch
                        {
                            requestSuccessfull = false;
                            logger.Error("Возникла ошибка получения данных от сервера для " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Возникла ошибка получения данных от сервера для " + currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }

                        if ((requestSuccessfull) && (! String.IsNullOrWhiteSpace(planDetailEntity.Id)))
                        {
                            logger.Info("обрабатывается " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("обрабатывается " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                            ReplicatePlan(planDetailEntity, maxDate);
                        }
                        else
                        {
                            logger.Error("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + " с ID = " + currentEntityId.ToString());                                  
                        }
                    }

                    if (currentEntity.Name.ToUpper() == "MKRK")
                    {
                        MkrkEntity mkrkEntity = new MkrkEntity();
                        bool requestSuccessfull = true;

                        try
                        {
                            mkrkEntity = client.GetMkrkEntity(currentEntityId.ToString());
                        }
                        catch
                        {
                            requestSuccessfull = false;
                            logger.Error("Возникла ошибка получения данных от сервера для " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Возникла ошибка получения данных от сервера для " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                        }

                        if ((requestSuccessfull) && (!String.IsNullOrWhiteSpace(mkrkEntity.Id)))
                        {
                            logger.Info("обрабатывается " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("обрабатывается " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                            ReplicateMkrk(mkrkEntity, maxDate);
                        }
                        else
                        {
                            logger.Error("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                        }
                    }

                    if (currentEntity.Name.ToUpper() == "MKRKSERVER")
                    {
                        MkrkEntity mkrkEntity = new MkrkEntity();
                        string[] tempstring = new string[2];
                        char[] comma = new char[1];
                        comma[0] = Convert.ToChar(";");
                        tempstring = currentEntityId.Split(comma, 2);
                        if (tempstring[0].Length == 0)
                        {tempstring[0] = "0";}
                        if (tempstring[1].Length == 0)
                        { tempstring[1] = "0"; }
                        bool requestSuccessfull = true;

                        try
                        {
                            mkrkEntity = client.GetMkrkEntityFromServerMkrk(tempstring[0], tempstring[1], maxDate);
                        }
                        catch
                        {
                            requestSuccessfull = false;
                            logger.Error("Возникла ошибка получения данных от сервера для " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Возникла ошибка получения данных от сервера для " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                        }

                        if (requestSuccessfull) 
                        {
                            logger.Info("обрабатывается " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("обрабатывается " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                            ReplicateMkrkFromServerMkrk(mkrkEntity, maxDate);
                        }
                        else
                        {
                            logger.Error("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + 
                                            " с ID = " + currentEntityId.ToString());
                            //Log.Write("Не получен ответ от сервера. Отмена вставки " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                        }
                    }

                    if (currentEntity.Name.ToUpper() == "SUBDIVISIONS")
                    {
                        SubdivisionEntity subdivisionEntity = client.GetSubdivisionEntity(currentEntityId.ToString());
                        logger.Info("обрабатывается " + currentEntity.Name + 
                                        " с ID = " + currentEntityId.ToString());
                        //Log.Write("обрабатывается " + currentEntity.Name + " с ID = " + currentEntityId.ToString());
                        //ReplicateSubdivision(subdivisionEntity, settings, maxDate);
                    }
                }
           }
            Console.WriteLine("========================");
            Console.ReadLine();
            //logfile.Close();
        }

    }
}
