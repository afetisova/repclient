﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepClient
{
    public class ClientSettings
    {
        public string ServiceUrl { get; set; }
        public uint ServicePort { get; set; }
        public string ClientProvider { get; set; }
        public string ClientConnectionString { get; set; }
        public bool Verbose { get; set; }
        public List<EntityCategory> EntityCategoryList { get; set; }
    }
}
