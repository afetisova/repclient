﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepClient
{
    public struct EntityCategory
    {
        public string Name;
        public DateTime? MaxDate;
        public EntityCategory(string name, DateTime? maxdate)
        {
            Name = name;
            MaxDate = maxdate;
        }
    }
}
